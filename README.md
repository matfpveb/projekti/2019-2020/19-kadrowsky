# Project 19-Kadrowsky

Veb aplikacija za kadrovsku sluzbu sa sledecim funkcionalnostima: 
odmori
bolovanja
evidencije taskova, 
posebne funkcionalnosti za hr za beleske o novim zaposlenima.

## Developers

- [Stefan Stanisic](https://gitlab.com/StefanStanisic)
- [Marko Serbic](https://gitlab.com/Sherba1)
- [Djordje Vujinovic](https://gitlab.com/djordjev96)
- [Aleksandar Mladenovic](https://gitlab.com/alexein777)

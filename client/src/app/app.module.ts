import { MatSelectModule } from '@angular/material/select';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatDividerModule } from '@angular/material/divider';
import { MatIconModule } from '@angular/material/icon';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DefaultModule } from './layouts/default/default.module';
import { MatDialogModule } from '@angular/material/dialog';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './utils/auth-interceptor';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';

import { AppComponent } from './app.component';
import { CompanyComponent } from './company/company.component';
import { ProfileComponent } from './profile/profile.component';
import { ReportsComponent } from './reports/reports.component';
import { EmployeesComponent } from './employees/employees.component';
import { CandidatesComponent } from './candidates/candidates.component';
import { DaysoffComponent } from './daysoff/daysoff.component';
import { TeamsComponent } from './teams/teams.component';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { ErrorPageComponent } from './app-routing/error-page/error-page.component';
import { LoginComponent } from './login/login.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { CandidateCreatorComponent } from './candidate-creator/candidate-creator.component';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { EmployedPipe } from './pipes/employed.pipe';
import { CandidateEditorComponent } from './candidate-editor/candidate-editor.component';
import { EmployeeCreatorComponent } from './employee-creator/employee-creator.component';
import { AgePipe } from './pipes/age.pipe';
import { UserEditorComponent } from './user-editor/user-editor.component';
import { ReportPreviewComponent } from './report-preview/report-preview.component';
import { ReportEditorComponent } from './report-editor/report-editor.component';
import { ApprovedPipe } from './pipes/approved.pipe';
import { MessageDialogComponent } from './message-dialog/message-dialog.component';
import { DayOffEditorComponent } from './day-off-editor/day-off-editor.component';

@NgModule({
  declarations: [
    AppComponent,
    CompanyComponent,
    ProfileComponent,
    ReportsComponent,
    EmployeesComponent,
    CandidatesComponent,
    DaysoffComponent,
    TeamsComponent,
    HomeComponent,
    RegisterComponent,
    ErrorPageComponent,
    LoginComponent,
    WelcomeComponent,
    CandidateCreatorComponent,
    ConfirmDialogComponent,
    EmployedPipe,
    CandidateEditorComponent,
    EmployeeCreatorComponent,
    AgePipe,
    UserEditorComponent,
    ReportPreviewComponent,
    ReportEditorComponent,
    ApprovedPipe,
    MessageDialogComponent,
    DayOffEditorComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    DefaultModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    MatIconModule,
    MatDividerModule,
    MatProgressSpinnerModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatGridListModule,
    MatSelectModule
  ],
  entryComponents: [
    ConfirmDialogComponent
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

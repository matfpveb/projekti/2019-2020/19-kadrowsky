import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DayOffEditorComponent } from './day-off-editor.component';

describe('DayOffEditorComponent', () => {
  let component: DayOffEditorComponent;
  let fixture: ComponentFixture<DayOffEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DayOffEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DayOffEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

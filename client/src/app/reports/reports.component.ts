import { User } from './../models/user';
import { ReportEditorComponent } from './../report-editor/report-editor.component';
import { ConfirmDialogComponent } from './../confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { ReportService } from './../services/report.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Report, ReportChanges, UserReports } from './../models/report';
import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit, OnDestroy {
  private activeSubscriptions: Subscription[] = [];
  public datePicker: Date = new Date();
  public reportText: string = '';
  public reports: Report[];
  public userReports: Report[];
  public allUsersReports: UserReports[];
  public uniqueUserIds: string[];
  public archivedReports: Report[];
  public archivedReportsForUser: Report[];
  public allUsersArchivedReports: UserReports[];
  public showArchive = false;
  public showArchiveText = 'Show archive';

  constructor(
    public authService: AuthenticationService,
    private reportService: ReportService,
    private dialog: MatDialog
  ) {
    // Ako su podaci o izvestajima vec dohvaceni nekim GET zahtevom,
    // samo ih dodeljujemo bez novog GET zahteva
    if (this.reportService.reportsSaved()) {
      this.assignReports();
    } else {
      // Inace, moramo da saljemo GET zahtev pre dodele
      this.assignReportsAfterUpdate();
    }

    // Isto za arhivirane izvestaje
    if (this.reportService.archivedReportsSaved()) {
      this.assignArchivedReports();
    } else {
      this.assignArchivedReportsAfterUpdate();
    }
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.activeSubscriptions.forEach(sub => sub.unsubscribe());
  }

  showReports(): boolean {
    return this.reports && this.reports.length > 0;
  }

  showArchivedReports(): boolean {
    return this.archivedReports && this.archivedReports.length > 0;
  }

  showArchivedReportsForUser(): boolean {
    return this.archivedReportsForUser && this.archivedReportsForUser.length > 0;
  }

  showUserReports(): boolean {
    return this.allUsersReports && this.allUsersReports.length > 0;
  }

  onShowArchive(): void {
    this.showArchive = !this.showArchive;
    this.showArchiveText = this.showArchive ? 'Hide archive' : 'Show archive';
  }

  getOwnReports(): Report[] {
    return this.reports.filter(report => {
      return report.user && report.user._id === this.authService.getLoggedUser()._id;
    });
  }

  getUserReports(): Report[] {
    return this.reports.filter(report => {
      return report.user && report.user.role !== 'owner';
    });
  }

  getReportsForAllUsers() {
    const reportsForAllUsers: UserReports[] = [];

    for (const report of this.userReports) {
      if (
        !reportsForAllUsers
          .map(userReport => userReport.user._id)
          .includes(report.user._id)
      ) {
        reportsForAllUsers.push({
          user: report.user,
          reports: this.getReportsForUserId(report.user._id)
        });
      }
    }

    return reportsForAllUsers;
  }

  getArchivedReportsForUserId(id: string): Report[] {
    return this.archivedReports.filter(report => {
      return report.user && report.user._id === id;
    });
  }

  getReportsForUserId(id: string): Report[] {
    return this.reports.filter(report => {
      return report.user && report.user._id === id;
    });
  }

  userHasReports(userReports: UserReports): boolean {
    return userReports.reports && userReports.reports.length > 0;
  }

  private assignReports(): void {
    const $reports = this.reportService.getReports()
      .subscribe(reports => {
        this.reports = reports;
        // Ako je ulogovan vlasnik, trebace nam informacije i o korisnickim izvestajima
        if (this.authService.isLoggedOwner()) {
          this.userReports = this.getUserReports();
          this.allUsersReports = this.getReportsForAllUsers();
        }
      });

    this.activeSubscriptions.push($reports);
  }

  private assignArchivedReports(): void {
    const $archivedReports = this.reportService.getArchivedReports()
      .subscribe(archivedReports => {
        this.archivedReports = archivedReports;
        // Ako je ulogovan korisnik, cuvamo njegovu arhivu posebno
        if (this.authService.isLoggedUser()) {
          this.archivedReportsForUser = this.getArchivedReportsForUserId(
            this.authService.getLoggedUser()._id
          );
        }
      });

    this.activeSubscriptions.push($archivedReports);
  }

  private assignReportsAfterUpdate(): void {
    // Salje se novi GET na /reports
    const $reports = this.reportService.refreshReports()
      .subscribe(reports => {
        this.reports = reports;
        // Ako je ulogovan vlasnik, trebace nam informacije i o korisnickim izvestajima
        if (this.authService.isLoggedOwner()) {
          this.userReports = this.getUserReports();
          this.allUsersReports = this.getReportsForAllUsers();
        }
      });
    this.activeSubscriptions.push($reports);
  }

  private assignArchivedReportsAfterUpdate(): void {
    // Salje se novi GET na /reports/archive
    const $archivedReports = this.reportService.refreshArchivedReports()
      .subscribe(archivedReports => {
        this.archivedReports = archivedReports;
        // Ako je ulogovan korisnik, cuvamo njegovu arhivu posebno
        if (this.authService.isLoggedUser()) {
          this.archivedReportsForUser = this.getArchivedReportsForUserId(
            this.authService.getLoggedUser()._id
          );
        }
      });

    this.activeSubscriptions.push($archivedReports);
  }

  private assignAllReportsAfterUpdate(): void {
    // 2 GET-a
    this.assignReportsAfterUpdate();
    this.assignArchivedReportsAfterUpdate();
  }

  onCreateReport(): void {
    const data = { report: this.reportText, week: this.datePicker };

    const $createdReport = this.reportService
      .createNewReport(data)
      .subscribe(createdReport => {
        console.log(createdReport.message);

        this.reportText = '';

        // Azuriramo izvestaje
        this.assignReportsAfterUpdate();
      });
    this.activeSubscriptions.push($createdReport);
  }

  hasReportText(): boolean {
    return this.reportText !== '';
  }

  hoverClass() {
    return this.hasReportText() ? 'kd-create-report' : 'kd-create-report-disabled';
  }

  onReportChanges(reportChanges: ReportChanges): void {
    // Proveravamo emitovani podatak koji u sebi sadrzi tacnu akciju
    // koju treba da preduzmemo nad izvestajem
    if (reportChanges.action === 'edit') {
      // PATCH
      const changeDialog = this.dialog.open(ReportEditorComponent, {
        data: {
          title: 'Edit report',
          message: 'Text change is neccessary (cancel if not needed)',
          oldText: reportChanges.oldText
        },
        width: '620px',
        height: '670px'
      });

      const $dialog = changeDialog
        .afterClosed()
        .subscribe(result => {
          if (result) {
            // result treba da budu izmene koje se vrse, oblika
            // { report: <novi_tekst>, week: <novi_datum> }
            const $updatedReport = this.reportService
              .editReportById(reportChanges._id, result)
              .subscribe(updatedReport => {
                console.log(updatedReport.message);
                window.alert('Report successfully updated');

                // Azuriramo izvestaje
                this.assignReportsAfterUpdate();
              });
            this.activeSubscriptions.push($dialog);
          }
        });
      this.activeSubscriptions.push($dialog);
    } else if (reportChanges.action === 'archive') {
      // DELETE (archive)
      const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
        data: {
          title: 'Archive report',
          message: 'Are you sure you want to archive report?'
        },
        width: '300px',
        height: '220px'
      });

      const $dialog = confirmDialog
        .afterClosed()
        .subscribe(result => {
          if (result) {
            const $archivedReport = this.reportService
              .archiveReportById(reportChanges._id)
              .subscribe(archivedReport => {
                console.log(archivedReport.message);

                // Azuriramo oba niza
                this.assignAllReportsAfterUpdate();
            });
            this.activeSubscriptions.push($archivedReport);
          }
        });
      this.activeSubscriptions.push($dialog);
    } else if (reportChanges.action === 'unarchive') {
      const $unarchivedReport = this.reportService
        .unarchiveReportById(reportChanges._id)
        .subscribe(unarchivedReport => {
          console.log(unarchivedReport.message);

          // Azuriramo oba niza
          this.assignAllReportsAfterUpdate();
        });
      this.activeSubscriptions.push($unarchivedReport);
    } else if (reportChanges.action === 'remove') {
      const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
        data: {
          title: 'Remove report',
          message: 'Are you sure you want to completely remove this report?'
        },
        width: '300px',
        height: '220px'
      });

      const $dialog = confirmDialog
        .afterClosed()
        .subscribe(result => {
          if (result) {
            const $deletedReport = this.reportService
              .removeReportById(reportChanges._id)
              .subscribe(deletedReport => {
                console.log(deletedReport.message);
                window.alert('Successfully removed report from archive');

                // Azuriramo samo arhivu
                this.assignArchivedReportsAfterUpdate();
            });
            this.activeSubscriptions.push($deletedReport);
          }
        });
      this.activeSubscriptions.push($dialog);
    } else {
      console.error('Unknown report-change action: ' + reportChanges.action);
    }
  }

  onClearArchive(): void {
    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: 'Clear archive',
        message: 'Do you want to remove all reports from archive?'
      },
      width: '300px',
      height: '220px'
    });

    const $dialog = confirmDialog
      .afterClosed()
      .subscribe(result => {
        if (result) {
          const $clearedArchive = this.reportService
            .removeReportsFromArchive()
            .subscribe(removedReports => {
              console.log(removedReports.message);

              // Azuriramo samo arhivu
              this.assignArchivedReportsAfterUpdate();
            });
          this.activeSubscriptions.push($clearedArchive);
        }
      });
    this.activeSubscriptions.push($dialog);
  }
}

import { Observable, throwError } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

export abstract class HttpErrorHandler {
  constructor(public router: Router) {}

  protected handleError() {
    return (err: HttpErrorResponse): Observable<never> => {
      const message = err.error.message ? err.error.message : err.message;
      const statusCode = err.status;
      console.log(err);

      if (err.error instanceof ErrorEvent) {
        // A client-side or network error occurred. Handle it accordingly.
        console.error('An error occurred:', message);
      } else {
        // The backend returned an unsuccessful response code.
        // The response body may contain clues as to what went wrong,
        this.router.navigate([ '/error', { message , statusCode } ]);
      }
      // return an observable with a user-facing error message
      return throwError('Something bad happened; please try again later.');
    };
  }
}

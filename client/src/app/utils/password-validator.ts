import { AbstractControl, FormGroup, ValidationErrors, ValidatorFn } from '@angular/forms';

export function passwordValidator(control: AbstractControl): ValidationErrors | null {
  if (control.value != null) {
    const hasCorrectLength = control.value.length >= 7;

    const lowerCaseRegex = '(.*)[a-z](.*)';
    const upperCaseRegex = '(.*)[A-Z](.*)';
    const specialCharRegex = '(.*)[!@#$%^&*+-_](.*)';
    const numberRegex = '(.*)[0-9](.*)';

    const hasLowerCase = control.value.match(lowerCaseRegex) !== null;
    const hasUpperCase = control.value.match(upperCaseRegex) !== null;
    const hasSpecialCharacter = control.value.match(specialCharRegex) !== null;
    const hasNumber = control.value.match(numberRegex) !== null;

    const correctPassword = hasCorrectLength && hasLowerCase && hasUpperCase &&
                            hasSpecialCharacter && hasNumber;

    return correctPassword ? null : {
      lengthError: !hasCorrectLength,
      lowerCaseError: !hasLowerCase,
      upperCaseError: !hasUpperCase,
      specialCharError: !hasSpecialCharacter,
      numberError: !hasNumber
    };
  }

  return null;
}

export function confirmPasswordKeysValidator(passwordKey: string, confirmPasswordKey: string):
  ValidationErrors | null {

  return (group: FormGroup) => {
    const passwordInput = group.controls[passwordKey];
    const confirmPasswordInput = group.controls[confirmPasswordKey];

    if (passwordInput.value !== confirmPasswordInput) {
      return confirmPasswordInput.setErrors({ notMatching: true});
    } else {
      return confirmPasswordInput.setErrors(null);
    }
  };
}

export function confirmPasswordValidator(group: FormGroup): ValidationErrors | null {
  const password = group.get('password').value;
  const confirmPassword = group.get('confirmPassword').value;

  return password === confirmPassword ? null : { notMatching: true };
}

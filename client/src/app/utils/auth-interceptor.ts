import { AuthenticationService } from './../services/authentication.service';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService: AuthenticationService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // Dohvatanje tokena iz lokalne memorije
    const token = this.authService.getToken();

    // Ako je token prisutan ...
    if (token) {
      // Kloniramo HTTP zaglavlja i dodajemo jedno zaglavlje viska pod nazivom 'Authorization'
      const cloned = req.clone({
        headers: req.headers.set('authorization', `Bearer ${token}`)
      });

      return next.handle(cloned);
    } else {
      // Ako nije, zahtev prolazi kroz server neizmenjen
      return next.handle(req);
    }
  }
}

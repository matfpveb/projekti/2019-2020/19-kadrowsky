import { AuthenticationService } from 'src/app/services/authentication.service';
import { MyErrorStateMatcher } from './../utils/error-state-matcher';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { passwordValidator, confirmPasswordValidator } from '../utils/password-validator';
import { Subscription } from 'rxjs';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {
  @ViewChild('regForm') registerFormDirective;
  public registerForm: FormGroup;
  private activeSubscriptions: Subscription[] = [];
  public matcher: MyErrorStateMatcher;

  private FORM_INITIAL_VALUE = {
    email: '',
    passwords: {
      password: '',
      confirmPassword: '',
    },
    companyName: '',
    package: ''
  };

  constructor(
    private authService: AuthenticationService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<RegisterComponent>)
  {
    this.registerForm = fb.group({
      email: ['', [Validators.required, Validators.email]],
      passwords: fb.group({
        password: ['', [Validators.required, Validators.minLength(7)]],
        confirmPassword: ['', [Validators.required]],
      }, { validator: confirmPasswordValidator} ),
      companyName: ['', Validators.required],
      package: ['', Validators.required]
    });
  }

  ngOnInit(): void {
  }

  ngOnDestroy() {
    this.activeSubscriptions.forEach(sub => sub.unsubscribe());
  }

  public get email() {
    return this.registerForm.get('email');
  }

  public get password() {
    return this.registerForm.get('passwords').get('password');
  }

  public get confirmPassword() {
    return this.registerForm.get('passwords').get('confirmPassword');
  }

  public get companyName() {
    return this.registerForm.get('companyName');
  }

  public get package() {
    return this.registerForm.get('package');
  }

  onSubmit(): void {
    const formData = this.registerForm.value;
    console.log(formData);

    // Post na /signup
    const sub = this.authService.createUserAccount(formData)
      .subscribe(user => {
        console.log('User account successfully created.\n' + user);
      });
    this.activeSubscriptions.push(sub);

    this.onResetForm();
    this.dialogRef.close();
  }

  onResetForm(): void {
    this.registerForm.reset(this.FORM_INITIAL_VALUE);
    this.registerFormDirective.resetForm();
  }

}

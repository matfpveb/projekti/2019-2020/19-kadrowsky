import { CompanyService } from './../services/company.service';
import { numberPattern } from './../shared/data/patterns';
import { FormControl, Validators, AbstractControl } from '@angular/forms';
import { Company } from './../models/company';
import { ConfirmDialogComponent } from './../confirm-dialog/confirm-dialog.component';
import { UserEditorComponent } from './../user-editor/user-editor.component';
import { MatDialog } from '@angular/material/dialog';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Subscription } from 'rxjs';
import { UserService } from './../services/user.service';
import { User } from './../models/user';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { nextTick } from 'process';
import { ConvertPropertyBindingResult } from '@angular/compiler/src/compiler_util/expression_converter';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {
  private activeSubscriptions: Subscription[] = [];
  public user: User;
  public company: Company;
  public companyPreview;
  public nameEdit: string;
  public showNameEditInput = false;
  public logoEdit: string;
  public showLogoEditInput = false;
  public maxNumOfEmployeesEdit: number;
  public showMaxNumOfEmployeesEditInput = false;
  public infoEdit: string;
  public showInfoEdit = false;

  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private userService: UserService,
    public authService: AuthenticationService,
    private companyService: CompanyService,
    private dialog: MatDialog
  ) {
    const $activatedRoute = this.route.paramMap
      .subscribe(params => {
        const userId = params.get('userId');

        const $user = this.userService.getUserById(userId)
          .subscribe(user => {
            this.user = user;

            if (this.authService.isLoggedOwner()) {
              this.company = this.authService.getLoggedUserCompany();
              this.companyPreview = { // duboko kopiranje
                name: this.company.name,
                logo: this.company.logo,
                owner: this.company.owner,
                numOfEmployees: this.company.numOfEmployees,
                maxNumOfEmployees: this.company.maxNumOfEmployees,
                info: this.company.info
              };

              this.nameEdit = this.company.name;
              this.logoEdit = this.company.logo;
              this.maxNumOfEmployeesEdit = this.company.maxNumOfEmployees;
              this.infoEdit = this.company.info;
            }
          });
        this.activeSubscriptions.push($user);
      });
    this.activeSubscriptions.push($activatedRoute);
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.activeSubscriptions.forEach(sub => sub.unsubscribe());
  }

  private assignUserAfterUpdate(): void {
    const $updatedUser = this.userService
      .getUserById(this.user._id)
      .subscribe(user => this.user = user);
    this.activeSubscriptions.push($updatedUser);
  }

  onEditUser(): void {
    const windowOptions = this.authService.isLoggedOwner()
    ? { width: '650px', height: '900px'}
    : { width: '650px', height: '450px'};

    const editEmployeeDialog = this.dialog
      .open(UserEditorComponent, {
        data: {
          title: 'Edit user info',
          subtitle: 'Select fields to edit (all fields are optional)',
          userId: this.user._id
        },
        ...windowOptions
      });

    const $dialog = editEmployeeDialog
      .afterClosed()
      .subscribe(result => {
        if (result) {
          // Refreshujemo TEKUCEG korisnika
          this.assignUserAfterUpdate();

          // Ako su se izmenili podaci trenutno ulogovanog korisnika,
          // moramo momentalno da ih zapamtimo
          if (result._id === this.authService.getLoggedUser()._id) {
            this.authService.addLoggedUser(result);
            localStorage.removeItem('loggedUser');
            localStorage.setItem('loggedUser', JSON.stringify(result));
          }

          // Azuriramo aktivne zaposlene (naloge) unutar servisa.
          // Prilikom konstrukcije klase Employees ona ce uvideti da vec postoje
          // korisnici pa ce izvrsiti dodelu (bez novog GET zahteva)
          this.userService.refreshUsers();
        }
      });
    this.activeSubscriptions.push($dialog);
  }

  onArchiveUser(): void {
    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: 'Archive employee',
        message: `Are you sure you want to archive employee account '${this.user.email}'?`
      },
      width: '300px',
      height: '220px'
    });

    const $dialog = confirmDialog
      .afterClosed()
      .subscribe(result => {
        if (result === true) {
          const $updatedUser = this.userService
            .archiveUserById(this.user._id)
            .subscribe(updatedUser => {
              console.log(updatedUser.message);
              window.alert('Employee account successfully moved to archive: '
                + updatedUser.user.email);

              // Obavestavamo preko komponentu Employees preko servisa da
              // preuzme nove podatke
              this.userService.refreshUsers();
              this.userService.refreshArchivedUsers();

              // Tekuci korisnik je obrisan pa ga postavljamo na null i preusmeravamo se
              // nazad na karticu employees
              this.router.navigateByUrl('/default/employees');
            });
          this.activeSubscriptions.push($updatedUser);
        }
      });
    this.activeSubscriptions.push($dialog);
  }

  onShowCompanyNameEditor(): void {
    this.showNameEditInput = !this.showNameEditInput;
  }

  onEditCompanyName(): void {
    this.companyPreview.name = this.nameEdit;
    this.showNameEditInput = false;
  }

  onCancelEditCompanyName(): void {
    this.nameEdit = this.companyPreview.name;
    this.showNameEditInput = false;
  }

  onShowCompanyLogoEditor(): void {
    this.showLogoEditInput = !this.showLogoEditInput;
  }

  onEditCompanyLogo(): void {
    this.companyPreview.logo = this.logoEdit;
    this.showLogoEditInput = false;
  }

  onCancelEditCompanyLogo(): void {
    this.logoEdit = this.companyPreview.logo;
    this.showLogoEditInput = false;
  }

  onShowCompanyMaxNumOfEmployeesEditor(): void {
    this.showMaxNumOfEmployeesEditInput = !this.showMaxNumOfEmployeesEditInput;
  }

  onEditCompanyMaxNumOfEmployees(): void {
    this.companyPreview.maxNumOfEmployees = this.maxNumOfEmployeesEdit;
    this.showMaxNumOfEmployeesEditInput = false;
  }

  onCancelEditCompanyMaxNumOfEmployees(): void {
    this.maxNumOfEmployeesEdit = this.companyPreview.maxNumOfEmployees;
    this.showMaxNumOfEmployeesEditInput = false;
  }

  onShowInfoEdit(): void {
    this.showInfoEdit = !this.showInfoEdit;
  }

  onEditCompanyInfo(): void {
    this.companyPreview.info = this.infoEdit;
    this.showInfoEdit = false;
  }

  onCancelEditCompanyInfo(): void {
    this.infoEdit = this.companyPreview.info;
    this.showInfoEdit = false;
  }

  onSaveChanges(): void {
    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: 'Save company changes',
        message: 'Do you want to save company changes?'
      },
      width: '300px',
      height: '230px'
    });

    const $dialog = confirmDialog
      .afterClosed()
      .subscribe(result => {
        if (result) {
          const $updatedCompany = this.companyService
            .updateCompanyById(this.company._id, this.companyPreview)
            .subscribe(updatedCompany => {
              console.log(updatedCompany.message);

              this.company = updatedCompany.company;
              console.log('Novi podaci posle azuriranja i dodele:');
              console.log(this.company);

              this.companyPreview = {
                name: updatedCompany.company.name,
                logo: updatedCompany.company.logo,
                owner: updatedCompany.company.owner,
                numOfEmployees: updatedCompany.company.numOfEmployees,
                maxNumOfEmployees: updatedCompany.company.maxNumOfEmployees,
                info: updatedCompany.company.info
              };

              // Pamtimo podatke u servisu
              this.authService.addLoggedUserCompany(updatedCompany.company);
              // localStorage.removeItem('loggedUserCompany');
              localStorage.setItem('loggedUserCompany', JSON.stringify(updatedCompany.company));

              // Saljemo novi GET na /company
              this.companyService.refreshCompany();

              window.alert('Company info successfully updated');
            });
          this.activeSubscriptions.push($updatedCompany);
        }
      });
    this.activeSubscriptions.push($dialog);
  }

  onUndoChanges(): void {
    this.companyPreview.name = this.company.name;
    this.nameEdit = this.company.name;

    this.companyPreview.logo = this.company.logo;
    this.logoEdit = this.company.logo;

    this.companyPreview.maxNumOfEmployees = this.company.maxNumOfEmployees;
    this.maxNumOfEmployeesEdit = this.company.maxNumOfEmployees;

    this.companyPreview.info = this.company.info;
    this.infoEdit = this.company.info;

    this.showNameEditInput = false;
    this.showLogoEditInput = false;
    this.showMaxNumOfEmployeesEditInput = false;
  }

}

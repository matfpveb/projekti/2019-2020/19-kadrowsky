import { Injectable } from '@angular/core';
import { Candidate } from '../models/candidate';
import { CANDIDATES_TEST } from '../shared/data/candidates';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor() { }

  getCandidates(): Candidate[] {
    return CANDIDATES_TEST;
  }
}

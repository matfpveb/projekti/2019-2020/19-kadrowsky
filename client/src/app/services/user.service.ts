import { AuthenticationService } from 'src/app/services/authentication.service';
import { UpdatedUsers } from './../models/update-response';
import { ResponseMessage } from './../models/response-message';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { User } from './../models/user';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpErrorHandler } from '../utils/http-error-handler';
import { HttpClient } from '@angular/common/http';
import { UpdatedUser } from '../models/update-response';

@Injectable({
  providedIn: 'root'
})
export class UserService extends HttpErrorHandler {
  private readonly usersURL = 'http://localhost:3000/users/';
  private readonly archivedUsersURL = 'http://localhost:3000/users/archive/';
  private users: Observable<User[]>;
  private archivedUsers: Observable<User[]>;

  constructor(
    private authService: AuthenticationService,
    private http: HttpClient,
    public router: Router
  ) {
    super(router);
    this.refreshUsers();
    this.refreshArchivedUsers();
  }

  getUsers(): Observable<User[]> {
    return this.users;
  }

  // GET /users
  refreshUsers(): Observable<User[]> {
    this.users = this.http
      .get<User[]>(this.usersURL)
      .pipe(catchError(super.handleError()));

    return this.users;
  }

  // POST /users
  createNewUser(formValue: any): Observable<UpdatedUser> {
    // NAPOMENA. Potreban refresh podataka (samo postojecih korisnika) nakon poziva
    return this.http
      .post<UpdatedUser>(this.usersURL, formValue)
      .pipe(catchError(super.handleError()));
  }

  // GET /users/:userId
  getUserById(id: string): Observable<User> {
    return this.http
      .get<User>(this.usersURL + id)
      .pipe(catchError(super.handleError()));
  }

  // PATCH /users/:userId
  updateUserById(id: string, options: any): Observable<UpdatedUser> {
    return this.http
      .patch<UpdatedUser>(this.usersURL + id, options)
      .pipe(catchError(super.handleError()));
  }

  // DELETE /users/:userId
  archiveUserById(id: string): Observable<UpdatedUser> {
    // NAPOMENA. Potreban refresh podataka nakon poziva
    return this.http
      .delete<UpdatedUser>(this.usersURL + id)
      .pipe(catchError(super.handleError()));
  }

  getArchivedUsers(): Observable<User[]> {
    return this.archivedUsers;
  }

  // GET /users/archive [SAMO OWNER]
  refreshArchivedUsers(): Observable<User[]> {
    if (this.authService.isLoggedOwner()) {
      this.archivedUsers = this.http
        .get<User[]>(this.archivedUsersURL)
        .pipe(catchError(super.handleError()));
    }

    return this.archivedUsers;
  }

  // DELETE /users/archive
  removeUsersFromArchive(): Observable<UpdatedUsers> {
    // NAPOMENA. Potreban refresh podataka (samo arhive) nakon poziva
    return this.http
      .delete<UpdatedUsers>(this.archivedUsersURL)
      .pipe(catchError(super.handleError()));
  }

  // GET /users/archive/:userId
  getArchivedUserById(id: string): Observable<User> {
    return this.http
      .get<User>(this.archivedUsersURL + id)
      .pipe(catchError(super.handleError()));
  }

  // PATCH /users/archive/:userId
  unarchiveUserById(id: string): Observable<UpdatedUser> {
    return this.http
      .patch<UpdatedUser>(this.archivedUsersURL + id, {})
      .pipe(catchError(super.handleError()));
  }

  // DELETE /users/archive/:userId
  removeArchivedUserById(id: string): Observable<UpdatedUser> {
    // NAPOMENA. Potreban refresh podataka (samo arhive) nakon poziva
    return this.http
      .delete<UpdatedUser>(this.archivedUsersURL + id)
      .pipe(catchError(super.handleError()));
  }

  usersSaved(): boolean {
    return this.users ? true : false;
  }

  archivedUsersSaved(): boolean {
    return this.archivedUsers ? true : false;
  }
}

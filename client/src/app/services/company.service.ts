import { UserService } from './user.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { UpdatedCompany } from './../models/update-response';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Company } from '../models/company';
import { HttpErrorHandler } from '../utils/http-error-handler';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class CompanyService extends HttpErrorHandler {
  private readonly companiesURL = 'http://localhost:3000/companies/';
  private company: Observable<Company>;
  private companyOwner: Observable<User>;

  constructor(
    private http: HttpClient,
    public router: Router,
    public authService: AuthenticationService,
    private userService: UserService
  ) {
    super(router);
    this.refreshCompany();
    this.refreshCompanyOwner();
  }

  getCompany(): Observable<Company> {
    return this.company;
  }

  getCompanyOwner(): Observable<User> {
    return this.companyOwner;
  }

  refreshCompany(): Observable<Company> {
    if (this.authService.getLoggedUserCompany()) {
      this.company = this.getCompanyById(this.authService.getLoggedUserCompany()._id);
    }
    return this.company;
  }

  refreshCompanyOwner(): Observable<User> {
    if (this.authService.getLoggedUserCompany()) {
      this.companyOwner = this.userService
        .getUserById(this.authService.getLoggedUserCompany().owner);
    }

    return this.companyOwner;
  }

  getCompanyById(id: string): Observable<Company> {
    return this.http
      .get<Company>(this.companiesURL + id)
      .pipe(catchError(super.handleError()));
  }

  updateCompanyById(id: string, options: any): Observable<UpdatedCompany> {
    return this.http
      .patch<UpdatedCompany>(this.companiesURL + id, options)
      .pipe(catchError(super.handleError()));
  }

  companySaved(): boolean {
    return this.company ? true : false;
  }

  companyOwnerSaved(): boolean {
    return this.companyOwner ? true : false;
  }
}

import { TestBed } from '@angular/core/testing';

import { DaysoffService } from './daysoff.service';

describe('DaysoffService', () => {
  let service: DaysoffService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DaysoffService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

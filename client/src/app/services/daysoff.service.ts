import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Observable } from 'rxjs';
import { DayOff, ApprovedDaysOff, CreatedDayOff } from './../models/day-off';
import { Injectable } from '@angular/core';
import { HttpErrorHandler } from '../utils/http-error-handler';
import { UpdatedDayOff } from '../models/update-response';

@Injectable({
  providedIn: 'root'
})
export class DaysoffService extends HttpErrorHandler {
  private readonly daysOffURL = 'http://localhost:3000/daysoff/';
  private readonly archivedDaysOffURL = 'http://localhost:3000/daysoff/archive/';
  private daysOff: Observable<ApprovedDaysOff>;

  constructor(
    private authService: AuthenticationService,
    private http: HttpClient,
    public router: Router
  ) {
    super(router);
    this.refreshDaysOff();
  }

  getDaysOff(): Observable<ApprovedDaysOff> {
    return this.daysOff;
  }

  // GET /daysoff
  refreshDaysOff(): Observable<ApprovedDaysOff> {
    this.daysOff = this.http
      .get<ApprovedDaysOff>(this.daysOffURL)
      .pipe(catchError(super.handleError()));

    return this.daysOff;
  }

  // POST /daysoff
  createNewDayOffRequest(data: any): Observable<CreatedDayOff> {
    // NAPOMENA. Potreban refresh i dana i usera posle kreiranja
    // data je oblika { dayType: 'sick_day' | 'vacation', date: Date }
    return this.http
      .post<CreatedDayOff>(this.daysOffURL, data)
      .pipe(catchError(super.handleError()));
  }

  // GET /daysoff/:dayoffId
  getDayOffById(id: string): Observable<DayOff> {
    return this.http
      .get<DayOff>(this.daysOffURL + id)
      .pipe(catchError(super.handleError()));
  }

  // PATCH /daysoff/:dayOffId
  updateDayOffById(id: string, options: any): Observable<UpdatedDayOff> {
    return this.http
      .patch<UpdatedDayOff>(this.daysOffURL + id, options)
      .pipe(catchError(super.handleError()));
  }

  // DELETE /daysoff/:dayOffId
  deleteDayOffById(id: string): Observable<UpdatedDayOff> {
    return this.http
      .delete<UpdatedDayOff>(this.daysOffURL + id)
      .pipe(catchError(super.handleError()));
  }

  daysOffSaved(): boolean {
    return this.daysOff ? true : false;
  }

  hasDaysOffLeft(): boolean {
    const nDayOff = this.authService.getLoggedUser().nDayOff;
    const maxDayOff = this.authService.getLoggedUser().maxDayOff;

    return nDayOff !== undefined &&
      maxDayOff !== undefined &&
      nDayOff < maxDayOff;
  }

  hasSickDaysOffLeft(): boolean {
    const nSickDays = this.authService.getLoggedUser().nSickDays;
    const maxSickDays = this.authService.getLoggedUser().maxSickDays;

    return nSickDays !== undefined &&
      maxSickDays !== undefined &&
      nSickDays < maxSickDays;
  }
}

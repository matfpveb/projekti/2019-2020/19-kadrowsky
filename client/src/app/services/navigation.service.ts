import { Injectable } from '@angular/core';
import { NavItem } from '../models/nav-item';
import { NAVITEMS } from '../shared/data/navigation-items';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {

  constructor() { }

  getNavItems(): NavItem[] {
    return NAVITEMS;
  }
}

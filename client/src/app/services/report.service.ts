import { UpdatedReport, UpdatedReports } from './../models/update-response';
import { catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Report } from '../models/report';
import { HttpErrorHandler } from '../utils/http-error-handler';

@Injectable({
  providedIn: 'root'
})
export class ReportService extends HttpErrorHandler {
  private readonly reportsURL = 'http://localhost:3000/reports/';
  private readonly archivedReportsURL = 'http://localhost:3000/reports/archive/';
  private reports: Observable<Report[]>;
  private archivedReports: Observable<Report[]>;

  constructor(
    public authService: AuthenticationService,
    public router: Router,
    private http: HttpClient
  ) {
    super(router);
    this.refreshReports();
    this.refreshArchivedReports();
  }

  getReports(): Observable<Report[]> {
    return this.reports;
  }

  // GET /reports
  refreshReports(): Observable<Report[]> {
    this.reports = this.http
      .get<Report[]>(this.reportsURL)
      .pipe(catchError(super.handleError()));

    return this.reports;
  }

  // POST /reports
  createNewReport(formValue: any): Observable<UpdatedReport> {
    // NAPOMENA. Potreban refresh podataka (samo postojecih izvestaja) nakon poziva
    return this.http
      .post<UpdatedReport>(this.reportsURL, formValue)
      .pipe(catchError(super.handleError()));
  }

  // GET /reports/:reportId
  getReportById(id: string): Observable<Report> {
    return this.http
      .get<Report>(this.reportsURL + id)
      .pipe(catchError(super.handleError()));
  }

  // PATCH /reports/:reportId
  editReportById(id: string, options: any): Observable<UpdatedReport> {
    // NAPOMENA. Potreban refresh podataka (samo postojecih izvestaja) nakon poziva
    return this.http
      .patch<UpdatedReport>(this.reportsURL + id, options)
      .pipe(catchError(super.handleError()));
  }

  // DELETE /reports/:reportId
  archiveReportById(id: string): Observable<UpdatedReport> {
    // NAPOMENA. Potreban refresh podataka nakon poziva
    return this.http
      .delete<UpdatedReport>(this.reportsURL + id)
      .pipe(catchError(super.handleError()));
  }

  getArchivedReports(): Observable<Report[]> {
    return this.archivedReports;
  }

  // GET /reports/archive [Vlasnik moze sve, a korisnik samo svoje izvestaje]
  refreshArchivedReports(): Observable<Report[]> {
    // Provera autorizacije se vrse pre poziva ove funkcije
    this.archivedReports = this.http
      .get<Report[]>(this.archivedReportsURL)
      .pipe(catchError(super.handleError()));

    return this.archivedReports;
  }

  // DELETE /reports/archive [SAMO VLASNIK]
  removeReportsFromArchive(): Observable<UpdatedReports> {
    // NAPOMENA. Potreban refresh podataka (samo arhive) nakon poziva
    // Provera autorizacije se vrse pre poziva ove funkcije
    return this.http
      .delete<UpdatedReports>(this.archivedReportsURL)
      .pipe(catchError(super.handleError()));
  }

  // GET /reports/archive/:reportId
  getArchivedReportById(id: string): Observable<Report> {
    // Provera autorizacije se vrse pre poziva ove funkcije
    return this.http
      .get<Report>(this.archivedReportsURL + id)
      .pipe(catchError(super.handleError()));
  }

  // PATCH /reports/archive/:reportId
  unarchiveReportById(id: string): Observable<UpdatedReport> {
    // NAPOMENA. Potreban refresh podataka nakon poziva
    // Provera autorizacije se vrse pre poziva ove funkcije
    return this.http
      .patch<UpdatedReport>(this.archivedReportsURL + id, {})
      .pipe(catchError(super.handleError()));
  }

  // DELETE /reports/archive/:reportId [SAMO VLASNIK]
  removeReportById(id: string): Observable<UpdatedReport> {
    // NAPOMENA. Potreban refresh podataka (samo arhive) nakon poziva
    // Provera autorizacije se vrse pre poziva ove funkcije
    return this.http
      .delete<UpdatedReport>(this.archivedReportsURL + id)
      .pipe(catchError(super.handleError()));
  }

  reportsSaved(): boolean {
    return this.reports ? true : false;
  }

  archivedReportsSaved(): boolean {
    return this.archivedReports ? true : false;
  }
}

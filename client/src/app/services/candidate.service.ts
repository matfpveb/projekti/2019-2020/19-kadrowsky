import { AuthenticationService } from 'src/app/services/authentication.service';
import { UpdatedCandidate, UpdatedCandidates } from './../models/update-response';
import { Candidate } from './../models/candidate';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpErrorHandler } from '../utils/http-error-handler';
import { catchError, map, switchMap, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CandidateService extends HttpErrorHandler {
  private readonly candidatesURL = 'http://localhost:3000/candidates/';
  private readonly archivedCandidatesURL = 'http://localhost:3000/candidates/archive/';
  private candidates: Observable<Candidate[]>;
  private archivedCandidates: Observable<Candidate[]>;

  constructor(
    public authService: AuthenticationService,
    public router: Router,
    private http: HttpClient,
  ) {
    super(router);
    this.refreshCandidates();
    this.refreshArchivedCandidates();
  }

  getCandidates(): Observable<Candidate[]> {
    return this.candidates;
  }

  // GET /candidates
  refreshCandidates(): Observable<Candidate[]> {
    this.candidates = this.http
      .get<Candidate[]>(this.candidatesURL)
      .pipe(catchError(super.handleError()));

    return this.candidates;
  }

  // POST /candidates
  createNewCandidate(formValue: any): Observable<UpdatedCandidate> {
    // NAPOMENA. Potreban refresh podataka (samo postojecih kandidata) nakon poziva
      return this.http
      .post<UpdatedCandidate>(this.candidatesURL, formValue)
      .pipe(catchError(super.handleError()));
  }

  // GET /candidates/:candidateId
  getCandidateById(id: string): Observable<Candidate> {
    return this.http
      .get<Candidate>(this.candidatesURL + id)
      .pipe(catchError(super.handleError()));
  }

  // PATCH /candidates/:candidateId
  editCandidateById(id: string, options: any): Observable<UpdatedCandidate> {
    // NAPOMENA. Potreban refresh podataka (samo postojecih kandidata) nakon poziva
    return this.http
      .patch<UpdatedCandidate>(this.candidatesURL + id, options)
      .pipe(catchError(super.handleError()));
  }

  // DELETE /candidates/:candidateId
  archiveCandidateById(id: string): Observable<UpdatedCandidate> {
    // NAPOMENA. Potreban refresh podataka nakon poziva
    return this.http
      .delete<UpdatedCandidate>(this.candidatesURL + id)
      .pipe(catchError(super.handleError()));
  }

  getArchivedCandidates(): Observable<Candidate[]> {
    return this.archivedCandidates;
  }

  // GET /candidates/archive [SAMO OWNER]
  refreshArchivedCandidates(): Observable<Candidate[]> {
    // Nema potrebe za proverom jer je cela kartica Candidates sakrivena od korisnika
    this.archivedCandidates = this.http
      .get<Candidate[]>(this.archivedCandidatesURL)
      .pipe(catchError(super.handleError()));

    return this.archivedCandidates;
  }

  // DELETE /candidates/archive
  removeCandidatesFromArchive(): Observable<UpdatedCandidates> {
    // NAPOMENA. Potreban refresh podataka (samo arhive) nakon poziva
    return this.http
      .delete<UpdatedCandidates>(this.archivedCandidatesURL)
      .pipe(catchError(super.handleError()));
  }

  // GET /candidates/archive/:candidateId
  getArchivedCandidateById(id: string): Observable<Candidate> {
    return this.http
      .get<Candidate>(this.archivedCandidatesURL + id)
      .pipe(catchError(super.handleError()));
  }

  // PATCH /candidates/archive/:candidateId
  unarchiveCandidateById(id: string): Observable<UpdatedCandidate> {
    // NAPOMENA. Potreban refresh podataka nakon poziva
    return this.http
      .patch<UpdatedCandidate>(this.archivedCandidatesURL + id, {})
      .pipe(catchError(super.handleError()));
  }

  // DELETE /candidates/archive/:candidateId
  removeCandidateById(id: string): Observable<UpdatedCandidate> {
    // NAPOMENA. Potreban refresh podataka (samo arhive) nakon poziva
    return this.http
      .delete<UpdatedCandidate>(this.archivedCandidatesURL + id)
      .pipe(catchError(super.handleError()));
  }

  candidatesSaved(): boolean {
    return this.candidates ? true : false;
  }

  archivedCandidatesSaved(): boolean {
    return this.archivedCandidates ? true : false;
  }
}

import { RegisterData } from './../models/register-data';
import { Company } from './../models/company';
import { CompanyService } from './company.service';
import { Router } from '@angular/router';
import { LoggedUser } from '../models/logged-user';
import { User } from '../models/user';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { shareReplay, tap, catchError, map, switchMap } from 'rxjs/operators';
import * as moment from 'moment';
import { HttpErrorHandler } from '../utils/http-error-handler';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService extends HttpErrorHandler {
  private readonly userSignupURL = 'http://localhost:3000/users/signup/';
  private readonly userLoginURL = 'http://localhost:3000/users/login/';
  private loggedUser: User = null;
  private loggedUserCompany: Company = null;

  constructor(
    private http: HttpClient,
    public router: Router
  ) {
    super(router);
    // Proveravamo da li je u trenutku kreiranja servisa vec prisutan
    // logovani korisnik u lokalnoj memoriji i ucitavamo ga ako jeste (isto za kompaniju)
    if (localStorage.getItem('loggedUser') && localStorage.getItem('loggedUserCompany')) {
      this.setLoggedUserByLocalStorageData();
      this.setLoggedUserCompanyByLocalStorageData();

      console.warn('User already logged in ... navigating to main page');

      this.router.navigateByUrl('/default');
    }
  }

  // POST /users/signup
  createUserAccount(formData): Observable<RegisterData> {
    const body = {
      email: formData.email,
      password: formData.passwords.password,
      companyName: formData.companyName,
      numOfEmployees: Number(formData.package)
    };

    return this.http
      .post<RegisterData>(this.userSignupURL, body)
      .pipe(catchError(super.handleError()));
  }

  // POST users/login
  login(email: string, password: string): Observable<LoggedUser> {
    // Brza provera da li je neko vec ulogovan
    if (this.isLoggedIn()) {
      console.log(`User ${this.getLoggedUser().email} already logged in: logging out ...`);

      this.logout();
    }

    const body = { email, password };

    // Nije identicno kao u tutorialu
    // Server vraca {user: User, token: string, expiresIn: string}
    return this.http
      .post<LoggedUser>(this.userLoginURL, body)
      .pipe(
        tap((loggedUser: LoggedUser) => {
          if (loggedUser && loggedUser.token) {
            console.log('Saving user data in localStorage and authService ...');
            // Server vraca vreme isteka u obliku "8h", tj. vrednost/jedinica
            const expData = loggedUser.expiresIn.trim().split('');
            const expVal = Number(expData[0]);
            const expUnit = expData[1] as moment.unitOfTime.DurationConstructor;
            const expiresAt = moment().add(expVal, expUnit);

            // Cuvamo podatke u lokalnoj memoriji
            localStorage.setItem('token', loggedUser.token);
            localStorage.setItem('expiresAt', JSON.stringify(expiresAt.valueOf()));
            localStorage.setItem('loggedUser', JSON.stringify(loggedUser.user));
            localStorage.setItem('loggedUserCompany', JSON.stringify(loggedUser.company));

            this.addLoggedUser(loggedUser.user);
            this.addLoggedUserCompany(loggedUser.company);
          } else {
            throwError(new Error('User data missing'));
          }
        }),
        shareReplay(),
        catchError(super.handleError()),
      );
  }

  logout() {
    this.removeLoggedUser();
    this.removeLoggedUserCompany();

    localStorage.removeItem('token');
    localStorage.removeItem('expiresAt');
    localStorage.removeItem('loggedUser');
    localStorage.removeItem('loggedUserCompany');

    this.router.navigateByUrl('/home');

    console.log('User successfully logged out');
  }

  isLoggedIn() {
    return this.getToken() && moment().isBefore(this.getExpiration());
  }

  isLoggedOut() {
    return !this.getToken() || !moment().isBefore(this.getExpiration());
  }

  getExpiration() {
    const expiration = localStorage.getItem('expiresAt');
    const expiresAt = JSON.parse(expiration);

    return moment(expiresAt);
  }

  getToken() {
    return localStorage.getItem('token');
  }

  addLoggedUser(user: User): void {
    this.loggedUser = user;
  }

  getLoggedUser(): User {
    return this.loggedUser;
  }

  removeLoggedUser(): void {
    this.loggedUser = null;
  }

  getLoggedUserFromLocalStorageData(): User {
    const user: User = JSON.parse(localStorage.getItem('loggedUser'));
    console.log('Logged user loaded from local storage');

    return user;
  }

  setLoggedUserByLocalStorageData(): void {
    this.loggedUser = this.getLoggedUserFromLocalStorageData();
  }

  addLoggedUserCompany(company: Company): void {
    this.loggedUserCompany = company;
  }

  getLoggedUserCompany(): Company {
    return this.loggedUserCompany;
  }

  removeLoggedUserCompany(): void {
    this.loggedUserCompany = null;
  }

  getLoggedUserCompanyFromLocalStorageData(): Company {
    const company: Company = JSON.parse(localStorage.getItem('loggedUserCompany'));
    console.log('Logged user company loaded from local storage');

    return company;
  }

  setLoggedUserCompanyByLocalStorageData(): void {
    this.loggedUserCompany = this.getLoggedUserCompanyFromLocalStorageData();
  }

  logLoginData() {
    console.log('Logged user:');
    console.log(this.loggedUser);
    console.log('Logged user company:');
    console.log(this.loggedUserCompany);
  }

  isLoggedOwner(): boolean {
    return this.loggedUser && this.loggedUser.role === 'owner';
  }

  isLoggedUser(): boolean {
    return this.loggedUser && this.loggedUser.role === 'basic';
  }
}

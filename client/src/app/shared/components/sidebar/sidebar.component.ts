import { Company } from './../../../models/company';
import { CompanyService } from './../../../services/company.service';
import { LoggedUser } from './../../../models/logged-user';
import { User } from './../../../models/user';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { NavItem } from '../../../models/nav-item';
import { NavigationService } from '../../../services/navigation.service';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  public navItems: NavItem[];
  public navCompany: NavItem;
  private readonly navItemRootRoute = '/default';
  private readonly companyIdHARDCODE = '5f6536cfaa3b5b2f20d95fd0';
  // public userCompany: Company;

  constructor(
    public authService: AuthenticationService,
    private navigationService: NavigationService,
    private companyService: CompanyService
  ) { }

  ngOnInit(): void {
    this.navItems = this.navigationService.getNavItems();
    this.navCompany = new NavItem('Company info', 'business', '/company', true, true);

    // this.companyService.getCompanyById(this.companyIdHARDCODE)
    //   .subscribe(company => this.userCompany = company);
  }

  getNavItemRoute(route: string): string {
    return this.navItemRootRoute + route;
  }

  get loggedUser(): User {
    return this.authService.getLoggedUser();
  }

  get loggedUserCompany(): Company {
    return this.authService.getLoggedUserCompany();
  }

  logLoginData() {
    this.authService.logLoginData();
  }
}

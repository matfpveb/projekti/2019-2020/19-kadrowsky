import { NavItem } from '../../models/nav-item';

export const NAVITEMS: NavItem[] = [
  new NavItem('Home', 'home', '/welcome', true, true),
  new NavItem('Dashboard', 'dashboard', '/dashboard', true, true),
  new NavItem('Employees', 'work', '/employees', true, true),
  new NavItem('Candidates', 'group_add', '/candidates', true, false),
  new NavItem('Reports', 'report', '/reports', true, true),
  new NavItem('Days off', 'work_off', '/daysoff', true, true),
  // new NavItem('Company', 'business', '/company'),
  // new NavItem('Profile', 'perm_identity', '/profile'),
  // new NavItem('Teams', 'group', '/teams'),
];

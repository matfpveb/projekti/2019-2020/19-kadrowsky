export const phonePattern = '^(([+][0-9]{3}[-\/ ]?[0-9]{2})|([0-9]{3}))[-\/ ]?[0-9]{4}[-\/ ]?[0-9]{3}$';
export const numberPattern = '^[0-9]*$';

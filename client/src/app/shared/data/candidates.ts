import { Candidate } from '../../models/candidate';

export const CANDIDATES_TEST: Candidate[] = [
  {
    _id: '21238655498991fegeg3',
    firstname: 'Pera',
    lastname: 'Peric',
    company: 'FIS',
    cv: 'link',
    email: 'pera94@gmail.com',
    phone: '+381607114567',
    mark: 334,
    comment: '',
    position: 'hr',
    is_deleted: false
  },
  {
    _id: 'savdskj2iu2iciep9j922942',
    firstname: 'Milojica',
    lastname: 'Milojkovic',
    company: 'Everseen',
    cv: 'link',
    email: 'milojica.mm@gmail.com',
    phone: '+381646566098',
    mark: 121,
    comment: 'Milojica je vredan!',
    position: 'dev',
    is_deleted: false
  },
  {
    _id: 'siuqh89evviowejv20j02neew9wwe',
    firstname: 'Spomenka',
    lastname: 'Lalic',
    company: 'Mega Computers Engineering',
    cv: 'link',
    email: 'spomenka@gmail.com',
    phone: '063/1121-334',
    mark: 2007,
    comment: '',
    position: 'cleaner',
    is_deleted: false
  },
  {
    _id: 'sacjf19jf0kffew93j3j3f3',
    firstname: 'Nikoleta',
    lastname: 'Milivojevic',
    company: 'DiscoTechs',
    cv: 'link',
    email: 'nikoleta_niki@yahoo.com',
    phone: '064-4443-187',
    mark: 309,
    comment: 'pravi dobru kafu xd',
    position: 'manager',
    is_deleted: false
  },
  {
    _id: 'sdajdsajqduqd1112irkf333',
    firstname: 'Rajko',
    lastname: 'Ralic',
    company: 'RT-RK',
    cv: 'link',
    email: 'rajko.ralea@gmail.com',
    phone: '+381659956776',
    mark: 21,
    comment: '',
    position: 'programmer',
    is_deleted: false
  },
];

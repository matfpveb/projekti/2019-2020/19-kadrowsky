import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { LoginComponent } from './../login/login.component';
import { RegisterComponent } from './../register/register.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    private router: Router,
    public dialog: MatDialog,
    private authService: AuthenticationService
  ) { }

  ngOnInit(): void {
    if (this.authService.isLoggedIn()) {
      console.warn('User already logged in ... navigating to main page');

      this.router.navigateByUrl('/default');
    }
  }

  openRegisterForm(): void {
    const windowOptions = { width: '500px', height: '680px' };
    this.dialog.open(RegisterComponent, windowOptions);
  }

  openLoginForm(): void {
    const windowOptions = { width: '400px', height: '360px' };
    this.dialog.open(LoginComponent, windowOptions);
  }
}

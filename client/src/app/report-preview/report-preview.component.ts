import { Report, ReportChanges } from './../models/report';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-report-preview',
  templateUrl: './report-preview.component.html',
  styleUrls: ['./report-preview.component.scss']
})
export class ReportPreviewComponent implements OnInit {
  @Input() report: Report;
  @Input() inArchive = false;
  @Input() isUserArchive = false;
  @Input() displayUser = false;
  @Output() reportChanges: EventEmitter<ReportChanges>;

  constructor() {
    this.reportChanges = new EventEmitter<ReportChanges>();
  }

  ngOnInit(): void {
  }

  onEditReport(): void {
    const data: ReportChanges = {
      _id: this.report._id,
      action: 'edit',
      oldText: this.report.report
    };

    this.reportChanges.emit(data);
  }

  onArchiveReport(): void {
    const data: ReportChanges = { _id: this.report._id, action: 'archive' };
    this.reportChanges.emit(data);
  }

  onUnarchiveReport(): void {
    const data: ReportChanges = { _id: this.report._id, action: 'unarchive' };
    this.reportChanges.emit(data);
  }

  onRemoveReport(): void {
    const data: ReportChanges = { _id: this.report._id, action: 'remove'};
    this.reportChanges.emit(data);
  }
}

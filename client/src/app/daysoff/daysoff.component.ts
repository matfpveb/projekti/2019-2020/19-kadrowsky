import { UserService } from './../services/user.service';
import { MessageDialogComponent } from './../message-dialog/message-dialog.component';
import { ConfirmDialogComponent } from './../confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { DaysoffService } from './../services/daysoff.service';
import { ApprovedDaysOff, DayOff, UserDaysOff } from './../models/day-off';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-daysoff',
  templateUrl: './daysoff.component.html',
  styleUrls: ['./daysoff.component.scss']
})
export class DaysoffComponent implements OnInit, OnDestroy {
  private activeSubscriptions: Subscription[] = [];
  public datePicker: Date = new Date();
  public selectedType = 'Vacation';
  public typeOptions = ['Sick day', 'Vacation'];
  public daysOff: ApprovedDaysOff;
  public allDaysOff: UserDaysOff[];

  constructor(
    public authService: AuthenticationService,
    private daysOffService: DaysoffService,
    private userService: UserService,
    public dialog: MatDialog
  ) {
    if (this.daysOffService.daysOffSaved()) {
      this.assignDaysOff();
    } else {
      this.assignDaysOffAfterUpdate();
    }
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.activeSubscriptions.forEach(sub => sub.unsubscribe());
  }

  getDaysOffForAllUsers(): UserDaysOff[] {
    const daysOffForAllUsers: UserDaysOff[] = [];
    const daysOffConcat: DayOff[] = this.daysOff.approved
      .concat(this.daysOff.notApproved);

    for (const dayOff of daysOffConcat) {
      if (
        !daysOffForAllUsers
          .map(userDayOff => userDayOff.user._id)
          .includes(dayOff.user._id)
      ) {
        daysOffForAllUsers.push({
          user: dayOff.user,
          daysOff: this.getDaysOffForUserId(daysOffConcat, dayOff.user._id)
        });
      }
    }

    return daysOffForAllUsers;
  }

  getDaysOffForUserId(daysOffConcat: DayOff[], id: string): DayOff[] {
    return daysOffConcat.filter(dayOff => dayOff.user._id === id);
  }

  userHasDaysOffRequests(userDaysOff: UserDaysOff): boolean {
    return userDaysOff.daysOff && userDaysOff.daysOff.length > 0;
  }

  showUserDaysOff(): boolean {
    return this.allDaysOff && this.allDaysOff.length > 0;
  }

  private assignDaysOff(): void {
    const $approved = this.daysOffService.getDaysOff()
      .subscribe(daysOff => {
        this.daysOff = daysOff;
        this.allDaysOff = this.getDaysOffForAllUsers();
      });
    this.activeSubscriptions.push($approved);
  }

  private assignDaysOffAfterUpdate(): void {
    const $approved = this.daysOffService.refreshDaysOff()
      .subscribe(daysOff => {
        this.daysOff = daysOff;
        this.allDaysOff = this.getDaysOffForAllUsers();
      });
    this.activeSubscriptions.push($approved);
  }

  onCreateRequest(): void {
    // console.log('hasDaysOffLeft: ' + this.daysOffService.hasDaysOffLeft());
    // console.log('hasSickDaysOffLeft: ' + this.daysOffService.hasSickDaysOffLeft());
    // console.log('Selecetd type: ' + this.selectedType);

    // console.log('nDaysOff: ' + this.authService.getLoggedUser().nDayOff);
    // console.log('maxDaysOff: ' + this.authService.getLoggedUser().maxDayOff);
    // console.log('nSickDays: ' + this.authService.getLoggedUser().nSickDays);
    // console.log('maxSickDays: ' + this.authService.getLoggedUser().maxSickDays);

    if (this.selectedType !== 'Vacation' && this.selectedType !== 'Sick day') {
      this.dialog.open(MessageDialogComponent, {
        data: {
          title: 'Warning',
          message: 'Select date type before sending request',
        }
      });

      return;
    }

    if (this.selectedType === 'Vacation' && !this.daysOffService.hasDaysOffLeft()) {
      this.dialog.open(MessageDialogComponent, {
        data: {
          title: 'Warning',
          message: 'You have no days off left!',
          matIcon: 'warning'
        }
      });

      return;
    } else if (this.selectedType === 'Sick day' && !this.daysOffService.hasSickDaysOffLeft()) {
      this.dialog.open(MessageDialogComponent, {
        data: {
          title: 'Warning',
          message: 'You have no sick days off left!',
          matIcon: 'warning'
        },
        width: '250px',
        height: '240px'
      });

      return;
    }

    const body = {
      date: this.datePicker,
      dayType: this.selectedType === 'Sick day' ? 'sick_day' : 'vacation'
    };

    const $createdDayOff = this.daysOffService
      .createNewDayOffRequest(body)
      .subscribe(createdDayOff => {
        console.log(createdDayOff.message);

        // Novi zahtev je napravljen pa moramo da saljemo novi GET
        this.assignDaysOffAfterUpdate();
      });
    this.activeSubscriptions.push($createdDayOff);
  }

  onEditRequest(dayOff: DayOff): void {
    // TODO: editovanje zahteva od strane korisnika
  }

  onRemoveRequest(dayOff: DayOff): void {
    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: 'Remove request',
        message: 'Do you want to remove day off request?',
        matIcon: 'warning'
      },
      width: '300px',
      height: '220px'
    });

    const $dialog = confirmDialog
      .afterClosed()
      .subscribe(result => {
        if (result) {
          const $deletedDayOff = this.daysOffService
            .deleteDayOffById(dayOff._id)
            .subscribe(deletedDayOff => {
              console.log(deletedDayOff.message);

              // Azuriramo ponovo podatke
              this.assignDaysOffAfterUpdate();
            });

          this.activeSubscriptions.push($deletedDayOff);
        }
      });
    this.activeSubscriptions.push($dialog);
  }

  showHistoryTable(): boolean {
    return this.daysOff && (
      this.daysOff.approved.length > 0 ||
      this.daysOff.notApproved.length > 0
    );
  }

  onApproveRequest(dayOff: DayOff): void {
    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: 'Approve day off request',
        message: 'Do you want to approve this day off request?'
      },
      width: '300px',
      height: '220px'
    });

    const $dialog = confirmDialog
      .afterClosed()
      .subscribe(result => {
        if (result) {
          const updateOptions = { approved: true };

          const $updatedRDayOff = this.daysOffService
            .updateDayOffById(dayOff._id, updateOptions)
            .subscribe(updatedDayOff => {
              console.log(updatedDayOff.message);

              // Azuriran je zahtev pa dohvatamo sve zahteve ponovo
              // Azurirani su i podaci o korisniku ciji je zahtev izmenjen!!!
              this.assignDaysOffAfterUpdate();

              // Vrlo neefikasno
              this.userService.refreshUsers();
            });
          this.activeSubscriptions.push($updatedRDayOff);
        }
      });
    this.activeSubscriptions.push($dialog);
  }
}

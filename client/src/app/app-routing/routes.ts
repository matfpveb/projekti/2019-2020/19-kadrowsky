import { WelcomeComponent } from './../welcome/welcome.component';
import { Routes } from '@angular/router';

import { DefaultComponent } from '../layouts/default/default.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { PostsComponent } from '../modules/posts/posts.component';
import { CompanyComponent } from '../company/company.component';
import { ProfileComponent } from '../profile/profile.component';
import { ReportsComponent } from '../reports/reports.component';
import { EmployeesComponent } from '../employees/employees.component';
import { CandidatesComponent } from '../candidates/candidates.component';
import { DaysoffComponent } from '../daysoff/daysoff.component';
import { TeamsComponent } from '../teams/teams.component';
import { HomeComponent } from '../home/home.component';
import { ErrorPageComponent } from './error-page/error-page.component';

export const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'error', component: ErrorPageComponent },
  {
    path: 'default',
    component: DefaultComponent,
    children: [
      { path: '', redirectTo: 'welcome', pathMatch: 'full' },
      { path: 'welcome', component: WelcomeComponent },
      { path: 'dashboard', component: DashboardComponent },
      { path: 'posts', component: PostsComponent },
      { path: 'company', component: CompanyComponent },
      { path: 'profile/:userId', component: ProfileComponent },
      { path: 'reports', component: ReportsComponent },
      { path: 'employees', component: EmployeesComponent },
      { path: 'candidates', component: CandidatesComponent },
      { path: 'daysoff', component: DaysoffComponent },
      // { path: 'teams', component: TeamsComponent },
    ],
  },
];

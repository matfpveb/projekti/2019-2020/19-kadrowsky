import { UserService } from './../services/user.service';
import { EmployeeCreatorComponent } from './../employee-creator/employee-creator.component';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { CandidateEditorComponent } from './../candidate-editor/candidate-editor.component';
import { UpdatedCandidate, UpdatedCandidates, UpdatedUser } from './../models/update-response';
import { ConfirmDialogComponent } from './../confirm-dialog/confirm-dialog.component';
import { CandidateCreatorComponent } from './../candidate-creator/candidate-creator.component';
import { MatDialog } from '@angular/material/dialog';
import { Subscription, Observable } from 'rxjs';
import { Candidate } from './../models/candidate';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { CandidateService } from '../services/candidate.service';

@Component({
  selector: 'app-candidates',
  templateUrl: './candidates.component.html',
  styleUrls: ['./candidates.component.scss'],
})
export class CandidatesComponent implements OnInit, OnDestroy {
  public candidates: Candidate[] = null;
  public archivedCandidates: Candidate[] = null;
  public showArchive = false;
  public showArchiveText = 'Show archive';
  private activeSubscriptions: Subscription[] = [];

  // TODO: ZAPOSLJAVANJE KANDIDATA
  constructor(
    public authService: AuthenticationService,
    private candidateService: CandidateService,
    private userService: UserService,
    private dialog: MatDialog
  ) {
    if (this.candidateService.candidatesSaved()) {
      // Ako su podaci o kandidatima vec dohvaceni nekim GET zahtevom,
      // samo ih dodeljujemo bez novog GET zahteva
      this.assignCandidates();
    } else {
      // Inace, moramo da saljemo GET zahtev pre dodele
      this.assignCandidatesAfterUpdate();
    }

    // Isto za arhivirane kandidate
    if (this.candidateService.archivedCandidatesSaved()) {
      this.assignArchivedCandidates();
    } else {
      this.assignArchivedCandidatesAfterUpdate();
    }
  }

  ngOnInit(): void {
    this.assignCandidates();
    this.assignArchivedCandidates();
  }

  ngOnDestroy(): void {
    this.activeSubscriptions.forEach(sub => sub.unsubscribe());
  }

  private assignCandidates(): void {
    const $candidates = this.candidateService.getCandidates()
      .subscribe(candidates => this.candidates = candidates);

    this.activeSubscriptions.push($candidates);
  }

  private assignArchivedCandidates(): void {
    const $archivedCandidates = this.candidateService.getArchivedCandidates()
      .subscribe(archivedCandidates => this.archivedCandidates = archivedCandidates);

    this.activeSubscriptions.push($archivedCandidates);
  }

  private assignCandidatesAfterUpdate(): void {
    // Salje se novi GET na /candidates
    const $candidates = this.candidateService.refreshCandidates()
      .subscribe(candidates => this.candidates = candidates);

    this.activeSubscriptions.push($candidates);
  }

  private assignArchivedCandidatesAfterUpdate(): void {
    // Salje se novi GET na /candidates/archive
    // Nema potreebe za proverom jer karticu Candidates ionako vidi samo vlasnik
    const $archivedCandidates = this.candidateService.refreshArchivedCandidates()
      .subscribe(archivedCandidates => this.archivedCandidates = archivedCandidates);

    this.activeSubscriptions.push($archivedCandidates);
  }

  private assignAllCandidatesAfterUpdate(): void {
    // 2 GET-a
    this.assignCandidatesAfterUpdate();
    this.assignArchivedCandidatesAfterUpdate();
  }

  onOpenCandidateCreationDialog(): void {
    const createCandidateDialog = this.dialog
      .open(CandidateCreatorComponent, {
        data: {
          title: 'Create candidate',
        },
        width: '600px',
        height: '500px'
      });

    const $createCandidateDialog = createCandidateDialog
      .afterClosed()
      .subscribe(result => {
        if (result) {
          // Ako je post zahtev bio uspesan dijalog je vratio true i potrebno
          // je azurirati niz kandidata
          this.assignCandidatesAfterUpdate();
        }
      });
    this.activeSubscriptions.push($createCandidateDialog);
  }

  onArchiveCandidate(candidate: Candidate): void {
    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: 'Archive candidate',
        message: `Are you sure you want to archive candidate ${candidate.firstname}` +
        ` ${candidate.lastname}?`
      },
      width: '300px',
      height: '220px'
    });

    const $dialog = confirmDialog
      .afterClosed()
      .subscribe(result => {
        if (result === true) {
          const $updatedCandidate = this.candidateService
            .archiveCandidateById(candidate._id)
            .subscribe(updatedCandidate => {
              console.log(updatedCandidate.message);
              window.alert(`Successfully archived candidate: ${candidate.firstname}` +
              ` ${candidate.lastname}`);

              // Azuriramo oba niza
              this.assignAllCandidatesAfterUpdate();
            });

          this.activeSubscriptions.push($updatedCandidate);
        }
      });
    this.activeSubscriptions.push($dialog);
  }

  showCandidates(): boolean {
    return this.candidates !== null && this.candidates.length > 0;
  }

  showArchivedCandidates(): boolean {
    return this.archivedCandidates !== null && this.archivedCandidates.length > 0;
  }

  onShowArchive(): void {
    this.showArchive = !this.showArchive;
    this.showArchiveText = this.showArchive ? 'Hide archive' : 'Show archive';
  }

  onUnarchiveCandidate(candidate: Candidate): void {
    const $updatedCandidate = this.candidateService
      .unarchiveCandidateById(candidate._id)
      .subscribe(updatedCandidate => {
        console.log(updatedCandidate.message);
        window.alert(`Candidate successfully removed from archive: ${candidate.firstname}` +
        ` ${candidate.lastname}`);

        // Azuriramo oba niza
        this.assignAllCandidatesAfterUpdate();
      });

    this.activeSubscriptions.push($updatedCandidate);
  }

  onRemoveCandidate(candidate: Candidate): void {
    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: 'Remove candidate',
        message: `Are you sure you want to remove candidate '${candidate.firstname}` +
        ` ${candidate.lastname}' from archive?`,
        matIcon: 'warning'
      },
      width: '330px',
      height: '250px'
    });

    const $dialog = confirmDialog
      .afterClosed()
      .subscribe(result => {
        if (result === true) {
          const $updatedCandidate = this.candidateService
            .removeCandidateById(candidate._id)
            .subscribe(updatedCandidate => {
              console.log(updatedCandidate.message);
              window.alert(`Successfully deleted candidate: ${candidate.firstname}` +
                ` ${candidate.lastname}`);

              // Azuriramo samo niz arhiviranih kandidata
              this.assignArchivedCandidatesAfterUpdate();
            });
          this.activeSubscriptions.push($updatedCandidate);
        }
      });
    this.activeSubscriptions.push($dialog);
  }

  onClearArchive(): void {
    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: 'Clear archive',
        message: 'Are you sure you want to remove all candidates from archive?',
        matIcon: 'warning'
      },
      width: '300px',
      height: '200px'
    });

    const $dialog = confirmDialog
      .afterClosed()
      .subscribe(result => {
        if (result === true) {
          const $updatedCandidates = this.candidateService
            .removeCandidatesFromArchive()
            .subscribe(updatedCandidates => {
              console.log(updatedCandidates.message);
              window.alert('Archive successfully cleared');

              // Azuriramo samo niz arhiviranih kandidata
              this.assignArchivedCandidatesAfterUpdate();
            });
          this.activeSubscriptions.push($updatedCandidates);
        }
      });
    this.activeSubscriptions.push($dialog);
  }

  onOpenCandidateEditDialog(candidate: Candidate): void {
    const editCandidateDialog = this.dialog
      .open(CandidateEditorComponent, {
        data: {
          title: 'Edit candidate',
          subtitle: 'All fields are optional',
          candidateId: candidate._id
        },
        width: '600px',
        height: '520px'
      });

    const $editCandidateDialog = editCandidateDialog
      .afterClosed()
      .subscribe(result => {
        if (result) {
          // Ako je patch zahtev bio uspesan dijalog je vratio true i potrebno
          // je azurirati niz kandidata
          this.assignCandidatesAfterUpdate();
        }
      });
    this.activeSubscriptions.push($editCandidateDialog);
  }

  onHireCandidate(candidate: Candidate): void {
    const uaCreationDialog = this.dialog.open(EmployeeCreatorComponent, {
      data: {
        email: candidate.email
      },
      width: '440px',
      height: '370px'
    });

    const $dialog = uaCreationDialog
      .afterClosed()
      .subscribe(result => {
        if (result) {
          // Dijalog je vratio korsnika (korisnik je uspesno kreiran)

          // Treba momentalno da azuriramo podatke o korisniku na osnovu
          // vec postojecih podataka koji su postojali o njemu kao o kandidatu
          const updateOptions = {
            firstname: candidate.firstname,
            lastname: candidate.lastname,
            phone: candidate.phone,
            position: candidate.position,
            cv: candidate.cv,
            joinDate: new Date().toDateString()
          };

          const $updatedUser = this.userService
            .updateUserById(result._id, updateOptions)
            .subscribe(updatedUser => {
              console.log(updatedUser.message);

              // Ulancano, nakon sto smo azurirali korisnika obavestavamo
              // komponentu Employees da se promenilo stanje preko servisa
              this.userService.refreshUsers();
            });
          this.activeSubscriptions.push($updatedUser);

          // Kandidat je postao zaposleni pa vise nije kandidat - brisemo ga
          const $deletedCandidate = this.candidateService
            .archiveCandidateById(candidate._id)
            .subscribe(deletedCandidate => {
            console.log(deletedCandidate.message);

            // Nakon sto smo arhivirali kandidata, treba da refreshujemo podatke
            // o svim kandidatima
            this.assignAllCandidatesAfterUpdate();
          });
          this.activeSubscriptions.push($deletedCandidate);
        }
      });
    this.activeSubscriptions.push($dialog);
  }
}

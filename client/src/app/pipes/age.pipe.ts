import { User, getUserAgeString } from './../models/user';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'age'
})
export class AgePipe implements PipeTransform {

  transform(user: User): unknown {
    return getUserAgeString(user);
  }

}

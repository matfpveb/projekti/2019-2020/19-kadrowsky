import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'username'
})
export class UsernamePipe implements PipeTransform {

  transform(email: string): unknown {
    return email.substr(0, email.indexOf('@'));
  }

}

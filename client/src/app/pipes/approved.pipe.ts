import { ApprovedDaysOff, DayOff } from './../models/day-off';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'approved'
})
export class ApprovedPipe implements PipeTransform {

  transform(daysOff: ApprovedDaysOff, approved: boolean): DayOff[] {
    if (approved) {
      return daysOff.approved;
    } else {
      return daysOff.notApproved;
    }
  }

}

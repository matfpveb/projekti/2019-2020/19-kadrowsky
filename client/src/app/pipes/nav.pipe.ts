import { NavItem } from './../models/nav-item';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nav'
})
export class NavPipe implements PipeTransform {

  transform(navItems: NavItem[], visibleToUser: boolean): unknown {
    if (visibleToUser) {
      return navItems.filter(navItem => navItem.visibleToUser === visibleToUser);
    } else {
      return navItems.filter(navItem => navItem.visibleToOwner === true);
    }
  }

}

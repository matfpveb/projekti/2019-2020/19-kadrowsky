import { phonePattern, numberPattern } from './../shared/data/patterns';
import { Subscription } from 'rxjs';
import { UserService } from './../services/user.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, OnInit, Inject, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-user-editor',
  templateUrl: './user-editor.component.html',
  styleUrls: ['./user-editor.component.scss']
})
export class UserEditorComponent implements OnInit, OnDestroy {
  private activeSubscriptions: Subscription[] = [];
  public editUserByOwnerForm: FormGroup;
  public editUserByUserForm: FormGroup;

  constructor(
    public authService: AuthenticationService,
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<UserEditorComponent>,
    private userService: UserService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.editUserByOwnerForm = this.fb.group({
      firstname: [''],
      lastname: [''],
      birthDate: [''],
      address: [''],
      phone: ['', [Validators.pattern(phonePattern)]],
      role: [''],
      cv: [''],
      nDayOff: ['', [Validators.pattern(numberPattern)]],
      maxDayOff: ['', [Validators.pattern(numberPattern)]],
      nSickDays: ['', [Validators.pattern(numberPattern)]],
      maxSickDays: ['', [Validators.pattern(numberPattern)]],
      joinDate: [''],
      position: ['']
    });

    this.editUserByUserForm = this.fb.group({
      firstname: [''],
      lastname: [''],
      birthDate: [''],
      address: [''],
      phone: ['', [Validators.pattern(phonePattern)]],
      cv: ['']
    });
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.activeSubscriptions.forEach(sub => sub.unsubscribe());
  }

  get phoneByOwner() {
    return this.editUserByOwnerForm.controls.phone;
  }

  get phoneByUser() {
    return this.editUserByUserForm.controls.phone;
  }

  get nDayOff() {
    return this.editUserByOwnerForm.controls.nDayOff;
  }

  get maxDayOff() {
    return this.editUserByOwnerForm.controls.maxDayOff;
  }

  get nSickDays() {
    return this.editUserByOwnerForm.controls.nSickDays;
  }

  get maxSickDays() {
    return this.editUserByOwnerForm.controls.maxSickDays;
  }

  submitByOwnerHoverClass() {
    return this.editUserByOwnerForm.valid ? 'kd-btn' : 'kd-btn-invalid';
  }

  submitByUserHoverClass() {
    return this.editUserByUserForm.valid ? 'kd-btn' : 'kd-btn-invalid';
  }

  private createEditOptions(formVal: any): any {
    const numberKeys = ['nDayOff', 'maxDayOff', 'nSickDays', 'maxSickDays'];
    const keys = Object.keys(formVal);
    const options = {};

    for (let i = 0; i < keys.length; ++i) {
      if (formVal[keys[i]] !== '') {
        if (numberKeys.includes(keys[i])) {
          // Pretvaramo stringovne brojeve u tipovne brojeve
          formVal[keys[i]] = Number(formVal[keys[i]]);
        }

        options[keys[i]] = formVal[keys[i]];
      }
    }

    return options;
  }

  onEditUserByOwner(): void {
    if (this.editUserByOwnerForm.invalid) {
      console.error('Invalid form data');
      return;
    }

    const editOptions = this.createEditOptions(this.editUserByOwnerForm.value);

    const $updatedUser = this.userService
      .updateUserById(this.data.userId, editOptions)
      .subscribe(updatedUser => {
        console.log(updatedUser.message);
        window.alert('Successfully updated user info');

        this.dialogRef.close(updatedUser.user);
      });
    this.activeSubscriptions.push($updatedUser);

    // this.dialogRef.close(false);
  }

  onEditUserByUser(): void {
    if (this.editUserByUserForm.invalid) {
      console.error('Invalid form data');
      return;
    }

    const editOptions = this.createEditOptions(this.editUserByUserForm.value);

    const $updatedUser = this.userService
      .updateUserById(this.data.userId, editOptions)
      .subscribe(updatedUser => {
        console.log(updatedUser.message);
        window.alert('Successfully updated user info');

        this.dialogRef.close(true);
      });
    this.activeSubscriptions.push($updatedUser);
  }

  onResetOwnerForm(): void {
    this.editUserByOwnerForm.reset({
      firstname: '',
      lastname: '',
      birthDate: '',
      address: '',
      phone: '',
      role: '',
      cv: '',
      nDayOff: 0,
      maxDayOff: 0,
      nSickDays: 0,
      maxSickDays: 0,
      joinDate: '',
      position: ''
    });
    this.dialogRef.close(false);
  }

  onResetUserForm(): void {
    this.editUserByUserForm.reset({
      firstname: '',
      lastname: '',
      birthDate: '',
      address: '',
      phone: '',
      role: '',
      cv: ''
    });
    this.dialogRef.close(false);
  }
}

import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, Inject, OnInit } from '@angular/core';

@Component({
  selector: 'app-report-editor',
  templateUrl: './report-editor.component.html',
  styleUrls: ['./report-editor.component.scss']
})
export class ReportEditorComponent implements OnInit {
  public newDatePicker: Date;
  public newReportText: string;

  constructor(
    private dialogRef: MatDialogRef<ReportEditorComponent>,
    @Inject(MAT_DIALOG_DATA) public data
  ) {
    this.newReportText = data.oldText;
  }

  ngOnInit(): void {
  }

  editDisable(): boolean {
    return this.newReportText === this.data.oldText;
  }

  editHoverClass(): string {
    return this.newReportText !== this.data.oldText ? 'kd-edit-report' : 'kd-edit-report-disabled';
  }

  onEdit(): void {
    const updates = { report: this.newReportText, week: this.newDatePicker };
    this.dialogRef.close(updates);
  }

  onCancel(): void {
    this.dialogRef.close(false);
  }
}

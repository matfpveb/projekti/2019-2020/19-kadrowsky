import { phonePattern, numberPattern } from './../shared/data/patterns';
import { Subscription } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CandidateService } from '../services/candidate.service';

@Component({
  selector: 'app-candidate-editor',
  templateUrl: './candidate-editor.component.html',
  styleUrls: ['./candidate-editor.component.scss']
})
export class CandidateEditorComponent implements OnInit, OnDestroy {
  private activeSubscriptions: Subscription[] = [];
  public editCandidateForm: FormGroup;
  private FORM_INITIAL_VALUE = {
    firstname: '',
    lastname: '',
    cv: '',
    email: '',
    phone: '',
    mark: 0,
    position: '',
    comment: ''
  };

  constructor(
    private fb: FormBuilder,
    private candidateService: CandidateService,
    public dialogRef: MatDialogRef<CandidateEditorComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.editCandidateForm = fb.group({
      firstname: [''],
      lastname: [''],
      cv: [''],
      email: ['', [Validators.email]],
      phone: ['', [Validators.pattern(phonePattern)]],
      mark: ['', [Validators.pattern(numberPattern)]],
      position: [''],
      comment: [''],
    });
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.activeSubscriptions.forEach(sub => sub.unsubscribe());
  }

  submitHoverClass() {
    return this.editCandidateForm.valid ? 'kd-btn' : 'kd-btn-invalid';
  }

  get firstname() {
    return this.editCandidateForm.controls.firstname;
  }

  get lastname() {
    return this.editCandidateForm.controls.lastname;
  }

  get cv() {
    return this.editCandidateForm.controls.cv;
  }

  get email() {
    return this.editCandidateForm.controls.email;
  }

  get phone() {
    return this.editCandidateForm.controls.phone;
  }

  get mark() {
    return this.editCandidateForm.controls.mark;
  }

  get position() {
    return this.editCandidateForm.controls.position;
  }

  get comment() {
    return this.editCandidateForm.controls.comment;
  }

  private createEditOptions(): any {
    const form = this.editCandidateForm.value;
    const keys = Object.keys(form);
    const options = {};

    for (let i = 0; i < keys.length; ++i) {
      if (form[keys[i]] !== '') {
        if (keys[i] === 'mark') {
          form[keys[i]] = Number(form[keys[i]]);
        }

        options[keys[i]] = form[keys[i]];
      }
    }

    return options;
  }

  onEditCandidate(): void {
    if (this.editCandidateForm.invalid) {
      console.error('Invalid form data');
      return;
    }

    const editOptions = this.createEditOptions();

    const $updatedCandidate = this.candidateService
      .editCandidateById(this.data.candidateId, editOptions)
      .subscribe(updatedCandidate => {
        const firstname = updatedCandidate.candidate?.firstname;
        const lastname = updatedCandidate.candidate?.lastname;

        console.log(updatedCandidate.message);
        window.alert(`Candidate ${firstname} ${lastname} successfully edited`);

        this.dialogRef.close(true);
      });
    this.activeSubscriptions.push($updatedCandidate);

    // this.dialogRef.close(false);
  }

  onReset(): void {
    this.editCandidateForm.reset(this.FORM_INITIAL_VALUE);
    this.dialogRef.close(false);
  }

}

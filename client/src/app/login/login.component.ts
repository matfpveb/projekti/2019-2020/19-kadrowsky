import { AuthenticationService } from 'src/app/services/authentication.service';
import { LoggedUser } from './../models/logged-user';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  public loginForm: FormGroup;
  private activeSubscriptions: Subscription[] = [];

  constructor(
    private fb: FormBuilder,
    private authService: AuthenticationService,
    private router: Router,
    public dialogRef: MatDialogRef<LoginComponent>
  ) {

    this.loginForm = fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.activeSubscriptions.forEach(sub => sub.unsubscribe());
  }

  public get email() {
    return this.loginForm.get('email');
  }

  public get password() {
    return this.loginForm.get('password');
  }

  onLogin(): void {
    const formData = this.loginForm.value;

    if (!formData.email || !formData.password) {
      console.error('Invalid form data');
      return;
    }

    // Dohvatanje korisnika (samo ako niko nije vec ulogovan lokalno)
    if (this.authService.isLoggedOut()) {
      const $loggedUser = this.authService
        .login(formData.email, formData.password)
        .subscribe((user: LoggedUser) => {
          console.log(`User ${user.user.email} logged in`);
        });
      this.activeSubscriptions.push($loggedUser);
    } else {
      console.warn('User already logged in ...');
    }

    this.router.navigateByUrl('/default');
    this.dialogRef.close();
  }

  onCancel(): void {
    this.loginForm.reset({ email: '', password: ''});
    this.dialogRef.close();
  }

  public submitHoverClass() {
    return this.loginForm.valid ? 'kd-login-btn' : 'kd-login-btn-invalid';
  }

}

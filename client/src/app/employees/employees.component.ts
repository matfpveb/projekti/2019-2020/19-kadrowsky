import { UserEditorComponent } from './../user-editor/user-editor.component';
import { ConfirmDialogComponent } from './../confirm-dialog/confirm-dialog.component';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { EmployeeCreatorComponent } from './../employee-creator/employee-creator.component';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { User, getUserAgeString } from './../models/user';
import { UserService } from './../services/user.service';
import { Component, OnInit, OnDestroy, Inject } from '@angular/core';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss']
})
export class EmployeesComponent implements OnInit, OnDestroy {
  public users: User[] = null;
  public archivedUsers: User[] = null;
  public showArchive = false;
  public showArchiveText = 'Show archive';
  private activeSubscriptions: Subscription[] = [];

  constructor(
    public authService: AuthenticationService,
    private userService: UserService,
    private dialog: MatDialog,
  ) {
    if (this.userService.usersSaved()) {
      // Ako su podaci o korisnicima vec dohvaceni nekim GET zahtevom,
      // samo ih dodeljujemo bez novog GET zahteva
      this.assignUsers();
    } else {
      // Inace, moramo da saljemo GET zahtev pre dodele
      this.assignUsersAfterUpdate();
    }

    // Isto za arhivirane korisnike
    if (this.userService.archivedUsersSaved()) {
      this.assignArchivedUsers();
    } else {
      this.assignArchivedUsersAfterUpdate();
    }
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.activeSubscriptions.forEach(sub => sub.unsubscribe());
  }

  private assignUsers(): void {
    const $users = this.userService.getUsers()
      .subscribe(users => this.users = users);

    this.activeSubscriptions.push($users);
  }

  private assignArchivedUsers(): void {
    // Vrsi DODELU samo ako je ulogovan vlasnik
    if (this.authService.isLoggedOwner()) {
      const $archivedUsers = this.userService.getArchivedUsers()
        .subscribe(archivedUsers => this.archivedUsers = archivedUsers);

      this.activeSubscriptions.push($archivedUsers);
    }
  }

  private assignUsersAfterUpdate(): void {
    // Salje se novi GET na /users
    const $users = this.userService.refreshUsers()
      .subscribe(users => this.users = users);

    this.activeSubscriptions.push($users);
  }

  private assignArchivedUsersAfterUpdate(): void {
    // Salje se novi GET na /users/archive
    // Vrsi dodelu samo ako je ulogovan vlasnik
    if (this.authService.isLoggedOwner()) {
      const $archivedUsers = this.userService.refreshArchivedUsers()
        .subscribe(archivedUsers => this.archivedUsers = archivedUsers);

      this.activeSubscriptions.push($archivedUsers);
    }
  }

  private assignAllUsersAfterUpdate(): void {
    // 2 GET-a
    this.assignUsersAfterUpdate();
    this.assignArchivedUsersAfterUpdate();
  }

  showUsers(): boolean {
    return this.users !== null && this.users.length > 0;
  }

  showArchivedUsers(): boolean {
    return this.archivedUsers !== null && this.archivedUsers.length > 0;
  }

  onShowArchive(): void {
    this.showArchive = !this.showArchive;
    this.showArchiveText = this.showArchive ? 'Hide archive' : 'Show archive';
  }

  onOpenEmployeeAccountCreationDialog(): void {
    const uaCreationDialog = this.dialog.open(EmployeeCreatorComponent, {
      width: '440px',
      height: '370px'
    });

    const $uaCreationDialog = uaCreationDialog
      .afterClosed()
      .subscribe(result => {
        if (result) {
          // Dijalog je vratio korisnika - treba jos da ga PATCH-ujemo
          // da bismo dodali datum kreiranja (zaposlenja) korisnika
          const updateOptions = { joinDate: new Date().toDateString() };

          const $updatedUser = this.userService
            .updateUserById(result._id, updateOptions)
            .subscribe(updatedUser => {
              console.log('Added users join date');

              // I tek sada azuriramo korisnike
              this.assignUsersAfterUpdate();
            });
          this.activeSubscriptions.push($updatedUser);
        }
      });
    this.activeSubscriptions.push($uaCreationDialog);
  }

  onArchiveEmployee(user: User): void {
    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: 'Archive employee',
        message: `Are you sure you want to archive employee account '${user.email}'?`
      },
      width: '300px',
      height: '220px'
    });

    const $dialog = confirmDialog
      .afterClosed()
      .subscribe(result => {
        if (result) {
          const $updatedUser = this.userService
            .archiveUserById(user._id)
            .subscribe(updatedUser => {
              console.log(updatedUser.message);
              window.alert('Employee account successfully moved to archive: '
                + updatedUser.user.email);

              // Azuriramo OBA niza
              this.assignAllUsersAfterUpdate();
            });
          this.activeSubscriptions.push($updatedUser);
        }
      });
    this.activeSubscriptions.push($dialog);
  }

  onUnarchiveEmployee(user: User): void {
    const $updatedUser = this.userService
      .unarchiveUserById(user._id)
      .subscribe(updatedUser => {
        console.log(updatedUser.message);
        window.alert(`User successfully removed from archive: ${updatedUser.user.email}`);

        // Azuriramo oba niza
        this.assignAllUsersAfterUpdate();
      });
    this.activeSubscriptions.push($updatedUser);
  }

  onRemoveEmployee(user: User): void {
    const confirmDialog = this.dialog
      .open(ConfirmDialogComponent, {
        data: {
          title: 'Remove employee account',
          message: `Are you sure you want to remove employee account '${user.email}' from archive?`
        },
        width: '330px',
        height: '220px'
      });

    const $dialog = confirmDialog
      .afterClosed()
      .subscribe(result => {
        if (result) {
          const $deletedUser = this.userService
            .removeArchivedUserById(user._id)
            .subscribe(deletedUser => {
              console.log(deletedUser.message);
              window.alert('Successfully removed employee account from archive');

              // Azuriramo samo arhivirane korisnike
              this.assignArchivedUsersAfterUpdate();
            });
          this.activeSubscriptions.push($deletedUser);
        }
      });
    this.activeSubscriptions.push($dialog);
  }

  onClearArchive(): void {
    const confirmDialog = this.dialog
      .open(ConfirmDialogComponent, {
        data: {
          title: 'Clear archive',
          message: 'Are you sure you want to clear archive? This operation will remove ' +
            'all employee accounts from database.'
        },
        width: '330px',
        height: '220px'
      });

    const $dialog = confirmDialog
      .afterClosed()
      .subscribe(result => {
        if (result) {
          const $updatedUsers = this.userService
            .removeUsersFromArchive()
            .subscribe(updatedUsers => {
              console.log(updatedUsers.message);
              window.alert('Successfully cleared employees archive');

              // Azuriramo arhivirane korisnike
              this.assignArchivedUsersAfterUpdate();
            });
        }
      });
    this.activeSubscriptions.push($dialog);
  }

  onOpenEmployeeEditDialog(user: User): void {
    const windowOptions = this.authService.isLoggedOwner()
      ? { width: '650px', height: '900px'}
      : { width: '650px', height: '450px'};

    const editEmployeeDialog = this.dialog
      .open(UserEditorComponent, {
        data: {
          title: 'Edit user info',
          subtitle: 'Select fields to edit (all fields are optional)',
          userId: user._id
        },
        ...windowOptions
      });

    const $dialog = editEmployeeDialog
      .afterClosed()
      .subscribe(result => {
        if (result) {
          // Azuriramo aktivne zaposlene (naloge)
          this.assignUsersAfterUpdate();
        }
      });
    this.activeSubscriptions.push($dialog);
  }
}

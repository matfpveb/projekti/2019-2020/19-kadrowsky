import { UserService } from './../services/user.service';
import { User } from './../models/user';
import { Subscription } from 'rxjs';
import { CompanyService } from './../services/company.service';
import { Company } from './../models/company';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss']
})
export class CompanyComponent implements OnInit, OnDestroy {
  private readonly companiesURL = 'http://localhost:3000/companies/';
  private activeSubscriptions: Subscription[] = [];
  public company: Company;
  public companyOwner: User;

  constructor(
    private companyService: CompanyService,
    private userService: UserService
  ) {
    if (this.companyService.companySaved()) {
      this.assignCompany();
    } else {
      this.assignCompanyAfterUpdate();
    }

    if (this.companyService.companyOwnerSaved()) {
      this.assingCompanyOwner();
    } else {
      this.assignCompanyOwnerAfterUpdate();
    }
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.activeSubscriptions.forEach(sub => sub.unsubscribe());
  }

  private assignCompany(): void {
    const $company = this.companyService.getCompany()
      .subscribe(company => this.company = company);

    this.activeSubscriptions.push($company);
  }

  private assignCompanyAfterUpdate(): void {
    const $company = this.companyService.refreshCompany()
      .subscribe(company => this.company = company);

    this.activeSubscriptions.push($company);
  }

  private assingCompanyOwner(): void {
    const $owner = this.companyService.getCompanyOwner()
      .subscribe(owner => this.companyOwner = owner);

    this.activeSubscriptions.push($owner);
  }

  private assignCompanyOwnerAfterUpdate(): void {
    const $owner = this.companyService.refreshCompanyOwner()
      .subscribe(owner => this.companyOwner = owner);

    this.activeSubscriptions.push($owner);
  }

  showCompanyInfo(): boolean {
    return this.company ? true : false;
  }

  showCompanyOwnerInfo(): boolean {
    return this.companyOwner !== undefined;
  }
}

import { Subscription } from 'rxjs';
import { UserService } from './../services/user.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, OnInit, OnDestroy, Inject, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-employee-creator',
  templateUrl: './employee-creator.component.html',
  styleUrls: ['./employee-creator.component.css']
})
export class EmployeeCreatorComponent implements OnInit, OnDestroy {
  public newUserAccountForm: FormGroup;
  private activeSubscriptions: Subscription[] = [];
  // @ViewChild('email') emailRef: ElementRef;

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    public dialogRef: MatDialogRef<EmployeeCreatorComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.newUserAccountForm = this.fb.group({
      // email: ['', [Validators.required, Validators.email]],
      email: new FormControl({value: ''}, [Validators.required, Validators.email]),
      password: ['', [Validators.required, Validators.minLength(7)]]
    });

    console.log('Dialog data: ' + this.data);
    if (this.data && this.data.email) {
      this.email.disable();
      this.email.setValue(this.data.email);
      // this.emailRef.nativeElement.value = this.data.email;
    } else {
      // this.emailRef.nativeElement.value = '';
      this.email.setValue('');
    }
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.activeSubscriptions.forEach(sub => sub.unsubscribe());
  }

  get f() {
    return this.newUserAccountForm.controls;
  }

  get email() {
    return this.newUserAccountForm.controls.email;
  }

  get password() {
    return this.newUserAccountForm.controls.password;
  }

  submitHoverClass() {
    return this.newUserAccountForm.valid ? 'kd-btn' : 'kd-btn-invalid';
  }

  onCreateNewUserAccount(): void {
    if (this.newUserAccountForm.invalid) {
      console.error('Invalid form data');
      return;
    }

    let body;
    if (this.data && this.data.email) {
      body = { email: this.data.email, password: this.f.password.value };
    } else {
      body = this.newUserAccountForm.value;
    }

    const $createdUser = this.userService
      .createNewUser(body)
      .subscribe(createdUser => {
        console.log(createdUser.message);
        window.alert('Successfully created new employee account');

        // Vracamo korisnika radi dohvatanja id-a
        this.dialogRef.close(createdUser.user);
      });
    this.activeSubscriptions.push($createdUser);
  }

  onClose(): void {
    this.newUserAccountForm.reset({ email: '', password: ''});
    this.dialogRef.close(false);
  }

  getEmailValue(): string {
    if (!this.data || !this.data.email) {
      return '';
    }

    return this.data.email;
  }

  shouldDisableEmail(): boolean {
    return this.data && this.data.email;
  }
}

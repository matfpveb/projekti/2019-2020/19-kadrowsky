export interface User {
  _id: string;
  email: string;
  password: string;
  company?: string;
  role?: string;
  nDayOff?: number;
  maxDayOff?: number;
  nSickDays?: number;
  maxSickDays?: number;
  firstname?: string;
  lastname?: string;
  joinDate?: string;
  position?: string;
  cv?: string;
  birthDate?: string;
  address?: string;
  phone?: string;
  team?: string;
  is_deleted?: boolean;
}

export function getUserAge(user: User): number | null {
  if (user.birthDate === '') {
    return null;
  }

  const birthDate = new Date(user.birthDate);
  const today = new Date();
  let age = today.getFullYear() - birthDate.getFullYear();

  const month = today.getMonth() - birthDate.getMonth();
  if (month < 0 || (month === 0 && today.getDate() < birthDate.getDate())) {
    --age;
  }

  return age;
}

export function getUserAgeString(user: User): string {
  const age = getUserAge(user);
  return age ? ('' + age) : '';
}

export function isOwner(user: User): boolean {
  return user.role === 'owner';
}

export function isEmployee(user: User): boolean {
  return user.role !== 'owner';
}

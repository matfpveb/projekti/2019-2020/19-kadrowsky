import { Company } from './company';
import { Report } from './report';
import { User } from './user';
import { Candidate } from './candidate';
import { DayOff } from './day-off';

export interface UpdatedCandidate {
  candidate: Candidate;
  message: string;
}

export interface UpdatedCandidates {
  candidates: Candidate[];
  message: string;
}

export interface UpdatedUser {
  user: User;
  message: string;
}

export interface UpdatedUsers {
  users: User[];
  message: string;
}

export interface UpdatedReport {
  report: Report;
  message: string;
}

export interface UpdatedReports {
  reports: Report[];
  message: string;
}

export interface UpdatedCompany {
  company: Company;
  message: string;
}

export interface UpdatedDayOff {
  dayOff: DayOff;
  message: string;
  user?: User;
}

import { Company } from './company';
import { User } from './user';

export interface LoggedUser {
  user: User;
  token: string;
  expiresIn: string;
  company: Company;
}

export interface RegisterData {
  email: string;
  password: string;
  companyName: string;
  numOfEmployees: number;
}

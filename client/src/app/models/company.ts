export interface Company {
  _id: string;
  name: string;
  owner: string;
  maxNumOfEmployees: number;
  numOfEmployees?: number;
  logo?: string;
  info?: string;
  is_deleted?: boolean;
}

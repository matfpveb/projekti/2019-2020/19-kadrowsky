import { User } from './user';

export interface Report {
  _id: string;
  report: string;
  week: Date;
  createdAt?: Date;
  updatedAt?: Date;
  user?: User;
  company?: string;
  team?: string;
  is_deleted?: boolean;
}

export interface ReportChanges {
  _id: string;
  action: string;
  oldText?: string;
  newText?: string;
  newWeek?: Date;
}

export interface UserReport {
  userId: string;
  report: Report;
}

export interface UserReports {
  user: User;
  reports: Report[];
}

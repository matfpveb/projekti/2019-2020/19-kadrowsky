export interface Candidate {
  _id: string;
  firstname: string;
  lastname: string;
  company: string;
  cv: string;
  email: string;
  phone: string;
  mark?: number;
  comment?: string;
  position?: string;
  is_deleted?: boolean;
}

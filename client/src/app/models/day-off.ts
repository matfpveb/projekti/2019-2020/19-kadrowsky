import { Company } from './company';
import { User } from './user';

export interface DayOff {
  _id: string;
  dayType: string;
  date: Date;
  user: User;
  company: Company;
  approved?: boolean;
  is_deleted?: boolean;
}

export interface ApprovedDaysOff {
  approved: DayOff[];
  notApproved: DayOff[];
}

export interface CreatedDayOff {
  nDaysOff: number;
  nSickDays: number;
  dayOff: DayOff;
  message?: string;
}

export interface UserDaysOff {
  user: User;
  daysOff: DayOff[];
}


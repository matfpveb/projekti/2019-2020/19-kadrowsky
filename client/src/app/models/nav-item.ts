export class NavItem {
  constructor(
    public name: string,
    public matIconName: string,
    public route: string,
    public visibleToOwner: boolean,
    public visibleToUser: boolean
  ) { }

  prependRoute(preRoute: string): string {
    // route vec sadrzi / na pocetku, pa nije potrebno dodavati /
    return preRoute + this.route;
  }
}

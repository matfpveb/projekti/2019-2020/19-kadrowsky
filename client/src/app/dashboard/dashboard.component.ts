import { Component, OnInit } from '@angular/core';
import { Candidate } from '../models/candidate';
import { DashboardService } from '../services/dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  candidates: Candidate[];

  constructor(private dashboardService: DashboardService) { }

  ngOnInit(): void {
    this.candidates = this.dashboardService.getCandidates();
  }


}

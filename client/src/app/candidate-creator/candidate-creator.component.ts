import { phonePattern, numberPattern } from './../shared/data/patterns';
import { UpdatedCandidate } from './../models/update-response';
import { Subscription } from 'rxjs';
import { CandidateService } from './../services/candidate.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, OnInit, ViewChild, OnDestroy, Inject } from '@angular/core';

@Component({
  selector: 'app-candidate-creator',
  templateUrl: './candidate-creator.component.html',
  styleUrls: ['./candidate-creator.component.scss']
})
export class CandidateCreatorComponent implements OnInit, OnDestroy {
  private activeSubscriptions: Subscription[] = [];
  public newCandidateForm: FormGroup;
  private FORM_INITIAL_VALUE = {
    firstname: '',
    lastname: '',
    cv: '',
    email: '',
    phone: '',
    mark: '',
    position: '',
    comment: ''
  };

  constructor(
    private fb: FormBuilder,
    private candidateService: CandidateService,
    public dialogRef: MatDialogRef<CandidateCreatorComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.newCandidateForm = fb.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      cv: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', [Validators.required, Validators.pattern(phonePattern)]],
      mark: ['', [Validators.pattern(numberPattern)]],
      position: [''],
      comment: ['', [Validators.maxLength(20)]],
    });
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.activeSubscriptions.forEach(sub => sub.unsubscribe());
  }

  submitHoverClass() {
    return this.newCandidateForm.valid ? 'kd-btn' : 'kd-btn-invalid';
  }

  get firstname() {
    return this.newCandidateForm.controls.firstname;
  }

  get lastname() {
    return this.newCandidateForm.controls.lastname;
  }

  get cv() {
    return this.newCandidateForm.controls.cv;
  }

  get email() {
    return this.newCandidateForm.controls.email;
  }

  get phone() {
    return this.newCandidateForm.controls.phone;
  }

  get mark() {
    return this.newCandidateForm.controls.mark;
  }

  get position() {
    return this.newCandidateForm.controls.position;
  }

  get comment() {
    return this.newCandidateForm.controls.comment;
  }

  onCreateNewCandidate(): void {
    if (this.newCandidateForm.invalid) {
      console.error('Invalid form data');
      return;
    }

    const $updatedCandidate = this.candidateService
      .createNewCandidate(this.newCandidateForm.value)
      .subscribe(updatedCandidate => {
        const firstname = updatedCandidate.candidate.firstname;
        const lastname = updatedCandidate.candidate.lastname;

        console.log(updatedCandidate.message);
        window.alert(`Candidate ${firstname} ${lastname} successfully created`);

        // Vracamo referencu na niz azuriranih kandidata
        // this.dialogRef.close(updatedCandidates); pre je dijalog vracao ceo niz azuriranih
        this.dialogRef.close(true);
      });
    this.activeSubscriptions.push($updatedCandidate);

    // this.dialogRef.close(null);
  }

  onReset(): void {
    this.newCandidateForm.reset(this.FORM_INITIAL_VALUE);
    this.dialogRef.close(false);
  }

}

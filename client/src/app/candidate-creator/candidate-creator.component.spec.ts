import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidateCreatorComponent } from './candidate-creator.component';

describe('CandidateCreatorComponent', () => {
  let component: CandidateCreatorComponent;
  let fixture: ComponentFixture<CandidateCreatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CandidateCreatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidateCreatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

exports.requestHandler = (type, router, id, hostname, requiredFields) => {
    if (id) {
      return ({
        type: type,
        url: 'http://' + hostname + ':3000/' + router + '/' + id + '/',
      });
    }
    else {
      return ({
        type: type,
        url: 'http://' + hostname + ':3000/' + router + '/',
        bodyRequiredFields: requiredFields
      });
    }
  };
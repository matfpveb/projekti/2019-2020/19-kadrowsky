const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const companySchema = new Schema({
    name: {
        type: String,
        required: true
    },
    logo: {
        type: String,
        default: ''
    },
    info: {
        type: String,
        default: ''
    },
    owner: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    maxNumOfEmployees: {
        type: Number,
        required: true
    },
    numOfEmployees: {
        type: Number,
        default: 1
    },
    is_deleted: {
        type: Boolean,
        default: false
    }
});

module.exports = mongoose.model('Company', companySchema);
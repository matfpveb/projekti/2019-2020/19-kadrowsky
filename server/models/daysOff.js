const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const dayOffSchema = new Schema({
    dayType: {
        // sick_day, vacation, sick_leave
        type: String,
        required: true
    },
    date: {
        type: Date,
        required: true
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    company: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Company'
    },
    approved: {
        type: Boolean,
        default: false
    },
    is_deleted: {
        type: Boolean,
        default: false
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('Day_off', dayOffSchema);
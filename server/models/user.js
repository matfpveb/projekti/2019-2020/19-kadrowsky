const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const validator = require('validator');
const bcrypt = require('bcryptjs');

const userSchema = new Schema({
    email: {
        type: String,
        required: true,
        lowercase: true,
        trim: true,
        validate(value) {
            if (!validator.isEmail(value)) {
                throw new Error('Email is invalid');
            }
        }
    },
    password: {
        type: String,
        required: true,
        minlength: 7,
    },
    role: {
        type: String,
        default: 'basic'
    },
    nDayOff: {
        type: Number,
        default: 0
    },
    maxDayOff: {
        type: Number,
        default: 0
    },
    nSickDays: {
        type: Number,
        default: 0
    },
    maxSickDays: {
        type: Number,
        default: 0
    },
    firstname: {
        type: String,
        default: ''
    },
    lastname: {
        type: String,
        default: ''
    },
    joinDate: {
        type: String,
        default: ''
    },
    position: {
        type: String,
        default: ''
    },
    cv: {
        type: String,
        default: ''
    },
    birthDate: {
        type: String,
        default: ''
    },
    address: {
        type: String,
        default: ''
    },
    phone: {
        type: String,
        default: ''
    },
    team: {
        type: String,
        default: ''
    },
    company: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Company'
    },
    is_deleted: {
        type: Boolean,
        default: false
    }
});

// Hash the plain text password before saving
userSchema.pre('save', async function (next) {
    const user = this

    if (user.isModified('password')) {
        user.password = await bcrypt.hash(user.password, 8)
    }

    next()
})

module.exports = mongoose.model('User', userSchema);

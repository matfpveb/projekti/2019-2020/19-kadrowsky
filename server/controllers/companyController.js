const Companies = require('../models/company');
const User = require('../models/user');
const handler = require('../handlers');


exports.companiesGetAll = async (req, res, next) => {
  try {
    const companies = await Companies.find({is_deleted: false})
    res.status(200).send(companies);
  } catch (e) {
    res.status(400).send(e)
  }
}


exports.companiesPost = async (req, res, next) => {
  try {
    if ( !(req.body.name) && !(req.body.info) ) {
      res.status(400).send("You must send all the fields.");
    }

    const newCompany = new Company({
      name: req.body.name,
      info: req.body.info,
      owner: req.userData.userId
    });

    // TODO: this used to be a $push; no idea why; check later
    await User.updateOne({_id: req.userData.userId}, { company: { companyId: company._id, role: 'owner' }})
    await newCompany.save();

  } catch (e) {
    res.status(400).send(e);
  }
}


exports.companiesPatch = async (req, res, next) => {
    res.status(403).json({
      message: 'Patch operation not supported on /companies'
    });
}


exports.companiesDeleteAll = async (req, res, next) => {
  try {
    const companies = await Companies.deleteMany({is_deleted: true});
    res.status(200).send(companies);
  } catch (e) {
    res.status(400).send(e);
  }
};


exports.getCompanyById = async (req, res, next) => {
  try {
    const company = await Companies.findOne({_id: req.params.companyId, is_deleted: false});

    if (!company) {
      res.status(400).send("Company not found");
    }

    res.status(200).send(company);
  } catch (e) {
    res.status(400).send(e);
  }
};


exports.postCompanyById = async (req, res, next) => {
    res.status(403).send({message: "Operation is not supported."});
};


exports.patchCompanyById = async (req, res, next) => {
  const updates = Object.keys(req.body);
  const company = await Companies.findById(req.params.companyId);

  try {
    if (req.user._id.equals(company.owner)) {
      updates.forEach((update) => company[update] = req.body[update]);
      if(req.file) {
        const port = process.env.PORT || 3000;
        company.logo = req.protocol + "://" + req.hostname + ':' + port + '/' + req.file.path;
      }
      await company.save();
      res.status(201).send({company, message: 'Company updated'});
    } else {
      res.status(400).send({message: "You don't have permission to make this operation"});
    }  
  } catch (e) {
    res.status(400).send(e);
  }
};


exports.deleteCompanyById = async (req, res, next) => {
  try {
    const company = await Companies.findOne({_id: req.params.companyId, is_deleted: false});

    if (!company){
      res.status(400).send({message: "There is no company with given id."});
    }

    company.is_deleted = true;
    // TODO: maybe block employees?
    await company.save();
  } catch (e) {
    res.status(400).send(e);
  }
};
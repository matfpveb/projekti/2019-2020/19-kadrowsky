const Report = require('../models/report');

// GET /reports
// TODO: /reports?limit=10&skip=0
//       /reports?sortBy=createdAt:desc
exports.reportsGetAll = async (req, res, next) => {
    try {
        const userRole = req.user.role;

        console.log('User company: ' + req.user.company);

        if (userRole === 'owner') {
            const reports = await Report.find({company: req.user.company, is_deleted: false})
              .populate('user');
            console.log('Reports: ' + reports);

            res.status(200).send(reports);
        } else if (userRole === 'basic') {
            const reports = await Report.find({user: req.user._id, company: req.user.company, is_deleted: false})
              .populate('user');

            res.status(200).send(reports);
        }
    } catch (e) {
        res.status(400).send(e);
    }
}

// POST /reports
exports.reportsPost = async (req, res, next) => {
    try {
        const report = new Report({
            report: req.body.report,
            week: req.body.week,
            user: req.user._id,
            team: req.user.team,
            company: req.user.company
        });

        await report.save();
        res.status(200).send({report, message: 'Successfully created report'});
    } catch (e) {
        res.status(400).send(e);
        console.log(e);
    }
}

// PATCH /reports
exports.reportsPatch = (req, res, next) => {
    res.status(403).json({
      message: 'Patch operation not supported on /reports'
    });
}

// DELETE /reports
exports.reportsDeleteAll = (req, res, next) => {
    res.status(403).json({
        message: 'Delete operation not supported on /reports'
    });
}

// GET /reprots/reportId
exports.getReportById = async (req, res, next) => {
    try {
        const report = await Report.findOne({_id: req.params.reportId, company: req.user.company})
          .populate('user');

        if (!report) {
            throw new Error('Report not found!');
        }

        if (req.user._id === report.user || req.user.role === 'owner') {
            res.status(200).send(report);
        } else {
            throw new Error('You are not allowed to perform this operation!');
        }
    } catch (e) {
        res.status(400).send(e);
    }
} 

// POST /reports/reportId
exports.postReportById = (req, res, next) => {
    res.status(403).json({
      message: 'Post operation not supported on /reports/' + req.params.reportId
    });
  }

  // PATCH /reports/reportId
exports.patchReportById = async (req, res, next) => {
    const updates = Object.keys(req.body);
    const allowedUpdatesReport = ['report', 'week'];

    const isValidOperation = updates.every((update) => allowedUpdatesReport.includes(update));

    if (!isValidOperation) {
      return res.status(400).send({ error: 'Invalid updates!' });
    }

    try {
        const report = await Report.findOne({_id: req.params.reportId, company: req.user.company});

        if (!report) {
            throw new Error('Report not found!');
        }

        if (req.user._id.equals(report.user) || req.user.role === 'owner') {
            updates.forEach((update) => report[update] = req.body[update]);
            await report.save();
            res.status(200).send({report, message: 'Report updated'});
        } else {
            throw new Error('You are not allowed to perform this operation!');
        }
    } catch (e) {
        res.status(400).send(e);
    }
}

// DELETE /reports/reportId
exports.deleteReportById = async (req, res, next) => {
    try {
        const report = await Report.findOne({_id: req.params.reportId, company: req.user.company});

        if (!report) {
            throw new Error('Report not found!');
        }

        report.is_deleted = true;

        await report.save();

        res.status(200).send({report, message: 'Report moved to archive'});
    } catch (e) {
        res.status(400).send(e);
    }
}

// ARCHIVE

// GET /reports/archive
exports.GetAllReportsFromArchive = async (req, res, next) => {
    try {
      const reports = await Report.find({company: req.user.company, is_deleted: true})
        .populate('user');
      res.status(200).send(reports);
    } catch (e) {
      res.status(400).send(e);
    }
  };


// DELETE /reports/archive
exports.DeleteAllReportsFromArchive = async (req, res, next) => {
    try {
      const reports = await Report.deleteMany({company: req.user.company, is_deleted: true})
      res.status(200).send({reports, message: 'Reports archive cleared'});
    } catch (e) {
      res.status(400).send(e);
    }
  };

// GET /reports/archive/reportId
exports.getReportByIdFromArchive = async (req, res, next) => {
    try {
      const report = await Report.findOne({_id: req.params.reportId, company: req.user.company, is_deleted: true})
        .populate('user');
  
      if (!report) {
        throw new Error('Report not found!');
      }
  
      res.status(200).send(report);
    } catch (e) {
      res.send(400).send(e);
    }
  };

// PATCH /reports/archive/reportId
// undelete report by id
exports.patchReportByIdFromArchive = async (req, res, next) => {
    try {
      const report = await Report.findOne({_id: req.params.reportId, company: req.user.company, is_deleted: true});
  
      if (!report) {
        throw new Error('Report not found!');
      }
  
      report.is_deleted = false;
  
      await report.save();
      res.status(200).send({report, message: 'Report moved from archive'});
    } catch (e) {
      res.send(400).send(e);
    }
  };
  
// DELETE /reports/archive/reportId
exports.deleteReportByIdFromArchive = async (req, res, next) => {
    try {
      const report = await Report.findOneAndDelete({ _id: req.params.reportId, company: req.user.company, is_deleted: true});
  
      if (!report) {
        throw new Error('Report not found!');
      }
  
      res.send({report, message: 'Report removed from archive'});
    } catch (e) {
      res.send(400).send(e);
    }
  };
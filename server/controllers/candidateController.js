const Candidate = require('../models/candidate');
const handler = require('../handlers');


// GET /candidates
exports.candidatesGetAll = async (req, res, next) => {
  try {
    const candidates = await Candidate.find({company: req.user.company, is_deleted: false});

    if (!candidates) {
      res.status(404).send('No candidates found');
    }

    res.status(200).send(candidates);
  } catch (e) {
    res.status(400).send(e);
  }
}

// POST /candidates
exports.candidatesPost = async (req, res, next) => {
  try {
    const candidate = new Candidate({
      firstname: req.body.firstname,
      lastname: req.body.lastname,
      cv: req.body.cv,
      company: req.user.company,
      email: req.body.email,
      phone: req.body.phone,
      mark: req.body.mark,
      comment: req.body.comment,
      position: req.body.position,
    });


    await candidate.save();
    res.status(200).send({candidate, message: 'Candidate successfully created'});
  } catch (e) {
    res.status(400).send(e);
  }
}

// PATCH /candidates
exports.candidatesPatch = (req, res, next) => {
    res.status(403).json({
      message: 'Patch operation not supported on /candidates'
    });
}

// DELETE /candidates
exports.candidatesDeleteAll = (req, res, next) => {
  res.status(403).json({
    message: 'Delete operation not supported on /candidates'
  });
}

// GET /candidates/candidateId
exports.getCandidateById = async (req, res, next) => {
  try {
    const candidate = await Candidate.find({company: req.user.company, _id: req.params.candidateId});

    if (!candidate) {
      res.status(404).send('Candidate not found');
    }

    res.status(200).send(candidate);
  } catch (e) {
    res.status(400).send(e);
  } 
}

// POST /candidates/candidateId
exports.postCandidateById = (req, res, next) => {
    res.status(403).json({
      message: 'Post operation not supported on /candidates/' + req.params.candidateId
    });
}

// PATCH /candidates/candidateId
exports.patchCandidateById = async (req, res, next) => {
  const updates = Object.keys(req.body);
  const candidate = await Candidate.findById(req.params.candidateId);

  if (!candidate) {
    res.status(404).send('Candidate not found');
  }

  try {
    if (req.user.company.equals(candidate.company)) {
      updates.forEach((update) => candidate[update] = req.body[update]);
      await candidate.save();
      
      res.status(201).send({candidate, message: 'Candidate successfully updated'});
    } else {
      res.status(400).send({message: "You don't have permission to make this operation"});
    }  
  } catch (e) {
    res.status(400).send(e);
  }
}

// DELETE /candidates/candidateId
exports.deleteCandidateById = async (req, res, next) => {
  try {
    const candidate = await Candidate.findOne({_id: req.params.candidateId, company: req.user.company, is_deleted: false});

    if (!candidate) {
      throw new Error('Candidate not found!');
    }

    candidate.is_deleted = true;

    await candidate.save();
    res.status(200).send({candidate, message: 'Candidate moved to archive' });
  } catch (e) {
    res.status(400).send(e);
  }
}

// Archive

// GET /candidates/archive
exports.GetAllCandidatesFromArchive = async (req, res, next) => {
  try {
    const candidates = await Candidate.find({company: req.user.company, is_deleted: true});
    res.status(200).send(candidates);
  } catch (e) {
    res.status(400).send(e);
  }
};

// TODO: is this smart?
// DELETE /candidates/archive
exports.DeleteAllCandidatesFromArchive = async (req, res, next) => {
  try {
    const candidates = await Candidate.deleteMany({company: req.user.company, is_deleted: true})
    res.status(200).send({candidates, message: 'Archive cleared'});
  } catch (e) {
    res.status(400).send(e);
  }
};

// GET /candidates/archive/candidateId
exports.getCandidateByIdFromArchive = async (req, res, next) => {
  try {
    const candidate = await Candidate.findOne({_id: req.params.candidateId, company: req.user.company, is_deleted: true});

    if (!candidate) {
      throw new Error('Candidate not found!');
    }

    res.status(200).send(candidate);
  } catch (e) {
    res.send(400).send(e);
  }
};

// PATCH /candidates/archive/candidateId
// this will unarchive candidate
exports.patchCandidateByIdFromArchive = async (req, res, next) => {
  try {
    const candidate = await Candidate.findOne({_id: req.params.candidateId, company: req.user.company, is_deleted: true});

    if (!candidate) {
      throw new Error('Candidate not found!');
    }

    candidate.is_deleted = false;

    await candidate.save();
    res.status(200).send({candidate, message: 'Candidate moved from archive'});
  } catch (e) {
    res.send(400).send(e);
  }
};

// DELETE /candidates/archive/candidateId
exports.deleteCandidateByIdFromArchive = async (req, res, next) => {
  try {
    const candidate = await Candidate.findOneAndDelete({ _id: req.params.candidateId, company: req.user.company, is_deleted: true});

    if (!candidate) {
      throw new Error('Candidate not found!');
    }

    res.status(200).send({candidate, message: 'Candidate deleted'});
  } catch (e) {
    res.send(400).send(e);
  }
};
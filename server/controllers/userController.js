const User = require('../models/user');
const Company = require('../models/company');

const bcrypt = require('bcryptjs');
const config = require('../config');
const jwt = require('jsonwebtoken');

// POST /users/signup
exports.userSignUp = async (req, res, next) => {
  if (!(req.body.email && req.body.password && req.body.companyName && req.body.numOfEmployees)) {
    res.status(400).send('You must fill all the fields');
  }

  const user = new User({
    email: req.body.email,
    password: req.body.password,
    role: 'owner'
  });
  const company = new Company({
    name: req.body.companyName, 
    maxNumOfEmployees: req.body.numOfEmployees,
    owner: user._id
  });

  user.company = company._id;
  
  try {
    await user.save();
    await company.save();
    res.status(201).send({user, company});
  } catch(e) {
    res.status(400).send(e);
  }
}

// POST /users/login
exports.userLogin = async (req, res, next) => {
  try {
    const user = await User.findOne({email: req.body.email});

    console.log(user);
  
    if (!user) {
      throw new Error('User doesn\'t exist!');
    }
  
    const isMatch = await bcrypt.compare(req.body.password, user.password);
  
    if (!isMatch) {
      throw new Error('Incorrect password')
    }
  
    const token = jwt.sign({ _id: user._id.toString() }, config.secretKey, { expiresIn: "8h" });

    const company = await Company.findOne({_id: user.company});

    res.status(200).send({user, token, expiresIn: "8h", company})
  } catch (e) {
    res.status(400).send(e);
  }
}

// GET /users
// TODO: /users?limit=10&skip=0
exports.usersGetAll = async (req, res, next) => {
  try {
    const users = await User.find({company: req.user.company, is_deleted: false})
    res.status(200).send(users);
  } catch (e) {
    res.status(400).send(e);
  }
}

//POST /users
exports.usersPost = async (req, res, next) => {
  try {
    const company = await Company.findOne({_id: req.user.company});

    if (company.numOfEmployees >= company.maxNumOfEmployees ) {
      throw new Error('Can\'t add more users!');
    }

    const newUser = new User({
      email: req.body.email,
      password: req.body.password,
      company: company._id
    });
    company.numOfEmployees += 1;
    
    await newUser.save();
    await company.save();

    res.status(201).send({ user: newUser, message: 'User successfully created'});
  } catch (e) {
    res.status(400).send(e);
  }
}

// GET users/userId
exports.getUserById = async (req, res, next) => {
  try {
    const user = await User.findOne({_id: req.params.userId, company: req.user.company});

    if (!user) {
      throw new Error('User not found!');
    }

    res.status(200).send(user);
  } catch (e) {
    res.send(400).send(e);
  }
}

// PATCH /users/userId
exports.patchUserById = async (req, res, next) => {
  const updates = Object.keys(req.body);
  const allowedUpdatesUser = ['firstname', 'lastname', 'cv', 'birthDate', 'address', 'phone'];
  const allowedUpdatesOwner = allowedUpdatesUser.concat(['role', 'nDayOff', 'maxDayOff', 'nSickDays', 'maxSickDays', 'joinDate', 'position']);
  
  if (req.user._id.equals(req.params.userId)) {
    const isValidOperation = updates.every((update) => allowedUpdatesUser.includes(update));

    if (!isValidOperation) {
      return res.status(400).send({ error: 'Invalid updates!' })
    }
  } else if (req.user.role === 'owner') {

    const isValidOperation = updates.every((update) => allowedUpdatesOwner.includes(update));

    if (!isValidOperation) {
      return res.status(400).send({ error: 'Invalid updates!' })  
    }

    if (updates.includes('role')) 
      req.body.role = req.body.role === 'owner' ? 'owner' : 'basic';

  } else {
    throw new Error('You are not allowed to do this operation!');
  }

  try {
    const user = await User.findOne({_id: req.params.userId, company: req.user.company});
    if (!user) {
      throw new Error('User not found!');
    }

    updates.forEach((update) => user[update] = req.body[update]);
    await user.save();

    res.send({user, message: 'User successfully updated'});
  } catch (e) {
    res.status(400).send(e);
  }
}

// DELETE /users/userId
exports.deleteUserById = async (req, res, next) => {
  try {
    const company = await Company.findOne({_id: req.user.company});
    const user = await User.findOne({_id: req.params.userId, company: req.user.company, is_deleted: false});

    if (!user) {
      throw new Error('User not found!');
    }
    
    user.is_deleted = true;
    company.numOfEmployees -= 1;
    
    await user.save();
    await company.save();
    res.status(200).send({user, message: 'User moved to archive'});
  } catch (e) {
    res.status(400).send(e);
  }
};

// GET /users/archive
// TODO: /users/archive?limit=10&skip=0
exports.usersGetAllFromArchive = async (req, res, next) => {
  try {
    const users = await User.find({company: req.user.company, is_deleted: true});
    res.status(200).send(users);
  } catch (e) {
    res.status(400).send(e);
  }
};

// DELETE /users/archive
exports.usersDeleteAllFromArchive = async (req, res, next) => {
  try {
    const users = await User.deleteMany({company: req.user.company, is_deleted: true})
    res.status(200).send({users, message: 'Successfully deleted users from archive'});
  } catch (e) {
    res.status(400).send(e);
  }
};

// GET /users/archive/userId
exports.getUserByIdFromArchive = async (req, res, next) => {
  try {
    const user = await User.findOne({_id: req.params.userId, company: req.user.company, is_deleted: true});

    if (!user) {
      throw new Error('User not found!');
    }

    res.status(200).send(user);
  } catch (e) {
    res.send(400).send(e);
  }
};

// PATCH /users/archive/userId
exports.patchUserByIdFromArchive = async (req, res, next) => {
  try {
    const user = await User.findOne({_id: req.params.userId, company: req.user.company, is_deleted: true});

    if (!user) {
      throw new Error('User not found!');
    }

    const company = await Company.findOne({_id: req.user.company});

    if (company.numOfEmployees >= company.maxNumOfEmployees ) {
      throw new Error('Can\'t add more users!');
    }

    user.is_deleted = false;
    company.numOfEmployees += 1;

    await user.save();
    await company.save();
    res.status(200).send({user, message: 'User moved from archive',  count: company.numOfEmployees});
  } catch (e) {
    res.send(400).send(e);
  }
};

// DELETE /users/archive/userId
exports.deleteUserByIdFromArchive = async (req, res, next) => {
  try {
    const user = await User.findOneAndDelete({ _id: req.params.userId, company: req.user.company, is_deleted: true});

    if (!user) {
      res.status(404).send();
    }

    res.send({ user, message: 'User deleted'});
  } catch (e) {
    res.send(400).send(e);
  }
};
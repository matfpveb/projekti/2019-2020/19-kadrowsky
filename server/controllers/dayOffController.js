const DaysOff = require('../models/daysOff');
const User = require('../models/user');

// GET /daysoff
exports.daysOffGetAll = async (req, res, next) => {
    const userRole = req.user.role;
    let approved = [];
    let notApproved = [];

    try {
        if (userRole === 'owner') {
            approved = await DaysOff.find({company: req.user.company, approved: true}).populate('user');
            notApproved = await DaysOff.find({company: req.user.company, approved: false}).populate('user');
        } else {
            approved = await DaysOff.find({company: req.user.company, user: req.user._id, approved: true}).populate('user');
            notApproved = await DaysOff.find({company: req.user.company, user: req.user._id, approved: false}).populate('user');
        }

        return res.status(200).send({
            'approved': approved,
            'notApproved': notApproved
        })
    } catch (e) {
        res.status(400).send(e);
        console.log(e);
    }
}

// POST /daysoff
exports.daysOffPost = async (req, res, next) => {
    try {
        const user = await User.findOne({_id: req.user._id});
        const dayType = await req.body.dayType;
        let dayoff;

        if (dayType === 'sick_day' && user.nSickDays < user.maxSickDays) {
            dayoff = new DaysOff({
                dayType: dayType,
                date: req.body.date,
                user: user._id,
                company: user.company
            });
            
            user.nSickDays += 1;

            await user.save();
            await dayoff.save();
        } else if (dayType === 'vacation' && user.nDayOff < user.maxDayOff) {
            dayoff = new DaysOff({
                dayType: dayType,
                date: req.body.date,
                user: user._id,
                company: user.company
            });

            user.nDayOff += 1;

            await user.save();
            await dayoff.save();
        } else {
          throw new Error('You don\'t have more days off!');
        }    
        
        res.status(200).send({
            "nDaysOff": user.nDayOff,
            "nSickDays": user.nSickDays,
            "dayOff": dayoff,
            "message": 'Day off request created'
        });
    } catch (e) {
        res.status(400).send(e);
        console.log(e);
    }
}

// PATCH /daysoff
exports.daysOffPatch = async (req, res, next) => {
    res.status(403).json({
        message: 'Patch operation not supported on /daysoff'
      });
}

// DELETE /daysoff
exports.daysOffDelete = async (req, res, next) => {
    // TODO:
    res.status(403).json({
        message: 'Patch operation not supported on /candidates'
      });
}


// GET /daysoff/dayOffId
exports.getDayOffById = async (req, res, next) => {
    try {
        const dayoff = await DaysOff.findOne({_id: req.params.dayOffId, company: req.user.company}).populate('user');

        if (!dayoff) {
            throw new Error('Can\'t find it!');
        }

        if (!(dayoff.user.equals(req.user._id) || req.user.role === 'owner')) {
            throw new Error('You don\'t have privilege to see this!');
        }

        res.status(200).send(dayoff);
    } catch (e) {
        res.status(400).send(e);
    }
}

// POST /daysoff/dayOffId
exports.postDayOffById = async (req, res, next) => {
    res.status(403).json({
        message: 'Post operation not supported on /daysoff/dayOffId'
    });
}

// PATCH /daysoff/dayOffId
exports.patchDayOffById = async (req, res, next) => {
    const updates = Object.keys(req.body);
    
    try {
        const dayoff = await DaysOff.findOne({_id: req.params.dayOffId, company: req.user.company});

        if (!dayoff) {
            res.status(404).send('Day off not found')
        }

        updates.forEach((update) => dayoff[update] = req.body[update]);

        await dayoff.save();
        res.status(200).send({dayoff, message: 'Day off updated'});
    } catch (e) {
        res.status(400).send(e);
    }
}

// DELETE /daysoff/dayOffId
exports.deleteDayOffById = async (req, res, next) => {
    try {
        const dayoff = await DaysOff.findOneAndDelete({_id: req.params.dayOffId, user: req.user._id});
        const user = await User.findOne({_id: req.user._id});

        if (!dayoff) 
            res.status(404).send('Day off not found!');
            
        if (dayoff.dayType === 'sick_day') {
            user.nSickDays += 1;
        } else if (dayoff.dayType === 'vacation') {
            user.nDayOff += 1;
        }

        await user.save()    
        res.status(200).send({user, dayoff, message: 'Day off deleted'});
    } catch (e) {
        res.status(400).send(e);
        console.log(e);
    }
}
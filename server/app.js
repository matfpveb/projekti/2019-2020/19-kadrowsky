const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');
const config = require('./config');
const cors = require('cors');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const candidateRouter = require('./routes/candidateRouter');
const companyRouter = require('./routes/companyRouter');
const dayRouter = require('./routes/dayOffRouter');
const reportRouter = require('./routes/reportRouter');

const app = express();

app.use(cors());

mongoose.connect(
  'mongodb+srv://admin:' + config.ATLAS_PW + '@kadrowsky-5kc0l.mongodb.net/test?retryWrites=true&w=majority',
  { useNewUrlParser: true, useUnifiedTopology: true }
);

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/', indexRouter);
app.use('/users', usersRouter);

app.use('/public', express.static(path.join(__dirname, 'public')));

app.use('/candidates', candidateRouter);
app.use('/companies', companyRouter);
app.use('/daysoff', dayRouter);
app.use('/reports', reportRouter);



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});



module.exports = app;

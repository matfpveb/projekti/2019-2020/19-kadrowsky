const express = require('express');
const bodyParser = require('body-parser');
const candidateRouter = express.Router();
const auth = require('../authenticate');

candidateRouter.use(bodyParser.json());

const candidateController = require('../controllers/candidateController');

candidateRouter.route('/')
.get(auth.verifyUser, auth.verifyOwner, candidateController.candidatesGetAll)
.post(auth.verifyUser, auth.verifyOwner, candidateController.candidatesPost)
.patch(auth.verifyUser, auth.verifyOwner, candidateController.candidatesPatch)
.delete(auth.verifyUser, auth.verifyOwner, candidateController.candidatesDeleteAll)

candidateRouter.route('/archive')
.get(auth.verifyUser, auth.verifyOwner, candidateController.GetAllCandidatesFromArchive)
.delete(auth.verifyUser, auth.verifyOwner, candidateController.DeleteAllCandidatesFromArchive)

candidateRouter.route('/:candidateId')
.get(auth.verifyUser, auth.verifyOwner, candidateController.getCandidateById)
.post(auth.verifyUser, auth.verifyOwner, candidateController.postCandidateById)
.patch(auth.verifyUser, auth.verifyOwner, candidateController.patchCandidateById)
.delete(auth.verifyUser, auth.verifyOwner, candidateController.deleteCandidateById)

candidateRouter.route('/archive/:candidateId')
.get(auth.verifyUser, auth.verifyOwner, candidateController.getCandidateByIdFromArchive)
.patch(auth.verifyUser, auth.verifyOwner, candidateController.patchCandidateByIdFromArchive)
.delete(auth.verifyUser, auth.verifyOwner, candidateController.deleteCandidateByIdFromArchive)

module.exports = candidateRouter;

const express = require('express');
const bodyParser = require('body-parser');
const reportRouter = express.Router();
const auth = require('../authenticate');

const reportController = require('../controllers/reportController');

reportRouter.use(bodyParser.json());

reportRouter.route("/")
.get(auth.verifyUser, reportController.reportsGetAll)
.post(auth.verifyUser, reportController.reportsPost)
.patch(auth.verifyUser, reportController.reportsPatch)
.delete(auth.verifyUser, auth.verifyOwner, reportController.reportsDeleteAll);

reportRouter.route("/archive")
.get(auth.verifyUser, reportController.GetAllReportsFromArchive)
.delete(auth.verifyUser, auth.verifyOwner, reportController.DeleteAllReportsFromArchive)

reportRouter.route('/:reportId')
.get(auth.verifyUser, reportController.getReportById)
.post(auth.verifyUser, reportController.postReportById)
.patch(auth.verifyUser, reportController.patchReportById)
.delete(auth.verifyUser, reportController.deleteReportById);

reportRouter.route("/archive/:reportId")
.get(auth.verifyUser, reportController.getReportByIdFromArchive)
.patch(auth.verifyUser, reportController.patchReportByIdFromArchive)
.delete(auth.verifyUser, auth.verifyOwner, reportController.deleteReportByIdFromArchive)

module.exports = reportRouter;
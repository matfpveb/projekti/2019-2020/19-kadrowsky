const express = require('express');
const bodyParser = require('body-parser');

const router = express.Router();
const auth = require('../authenticate');

const userController = require('../controllers/userController');

router.use(bodyParser.json());

// Registering new user and company
router.post('/signup', userController.userSignUp);

// Login
router.post('/login', userController.userLogin);

// Logout is maybe better to implement in client by deleting token 
// router.get('/logout', (req, res) => {
//   req.logout();
//   res.redirect('/');
// })

// Get all users of company 
router.route('/')
.get(auth.verifyUser, userController.usersGetAll)
// Adding user by company owner
.post(auth.verifyUser, auth.verifyOwner, userController.usersPost);


router.route('/archive')
.get(auth.verifyUser, auth.verifyOwner, userController.usersGetAllFromArchive)
.delete(auth.verifyUser, auth.verifyOwner, userController.usersDeleteAllFromArchive);

router.route('/:userId')
.get(auth.verifyUser, userController.getUserById)
.patch(auth.verifyUser, userController.patchUserById)
// Deleting a user by company owner
.delete(auth.verifyUser, auth.verifyOwner, userController.deleteUserById);

router.route('/archive/:userId')
.get(auth.verifyUser, auth.verifyOwner, userController.getUserByIdFromArchive)
.patch(auth.verifyUser, auth.verifyOwner, userController.patchUserByIdFromArchive)
.delete(auth.verifyUser, auth.verifyOwner, userController.deleteUserByIdFromArchive);

module.exports = router;

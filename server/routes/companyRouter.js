const express = require('express');
const bodyParser = require('body-parser');
const companyRouter = express.Router();
const auth = require('../authenticate');
const multer = require('multer');
const path = require('path');

companyRouter.use(bodyParser.json());

const companyController = require('../controllers/companyController');

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
      cb(null, './public/images');
    },
    filename: function(req, file, cb) {
      cb(null, new Date().toISOString() + path.extname(file.originalname));
    }
  });
  
const fileFilter = (req, file, cb) => {
    // reject a file
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png' || file.mimetype === 'image/jpg') {
      cb(null, true);
    } else {
      cb(null, false);
    }
  };
  
const upload = multer({
    storage: storage,
    limits: {
      fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
  });

companyRouter.route('/')
// ovo mozemo samo mi, glavni administratori
.get(auth.verifyUser, auth.verifyAdmin, companyController.companiesGetAll)
// Za sada brisemo
// .post(auth.verifyUser, companyController.companiesPost)
// Niko ne moze da menja firme
.patch(auth.verifyUser, auth.verifyAdmin, companyController.companiesPatch)
// Ovu operaciju mozemo i da izbrisemo, jer ni mi ne zelimo operaciju koja brise sve firme
.delete(auth.verifyUser, auth.verifyAdmin, companyController.companiesDeleteAll)

companyRouter.route('/:companyId')
// ovo omogucavamo nama
.get(auth.verifyUser, companyController.getCompanyById)
.post(companyController.postCompanyById)
// firmu ce moci da menja onaj ko ju je kreirao
.patch(auth.verifyUser, auth.verifyOwner, upload.single('companyLogo'), companyController.patchCompanyById)
// ovde moramo da razmislimo, ukoliko vlasnik obrise firmu, sta cemo sa njenim korisnicima?
.delete(auth.verifyUser, auth.verifyOwner, companyController.deleteCompanyById);

module.exports = companyRouter;
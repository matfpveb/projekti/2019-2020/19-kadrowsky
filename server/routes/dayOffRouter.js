const express = require('express');
const bodyParser = require('body-parser');
const dayRouter = express.Router();
const auth = require('../authenticate');
const dayOffController = require('../controllers/dayOffController');

dayRouter.use(bodyParser.json());

dayRouter.route('/')
.get(auth.verifyUser, dayOffController.daysOffGetAll)
.post(auth.verifyUser, dayOffController.daysOffPost)
.patch(auth.verifyUser, dayOffController.daysOffPatch)
.delete(auth.verifyUser, dayOffController.daysOffDelete);

dayRouter.route('/:dayOffId')
.get(auth.verifyUser, dayOffController.getDayOffById)
.post(auth.verifyUser, dayOffController.postDayOffById)
.patch(auth.verifyUser, auth.verifyOwner, dayOffController.patchDayOffById)
.delete(auth.verifyUser, dayOffController.deleteDayOffById);

module.exports = dayRouter;
const User = require('./models/user');
const jwt = require('jsonwebtoken');
const config = require('./config');

exports.verifyUser = async (req, res, next) => {
    try {
        const token = req.headers.authorization.split(" ")[1];
        const decoded = jwt.verify(token, config.secretKey);
        const user = await User.findOne({_id: decoded._id});

        req.token = token;
        req.user = user;
        next();
    } catch (error) {
        return res.status(401).json('Auth failed');
    }
};

exports.verifyAdmin = (req, res, next) => {
    User.findOne({_id: req.user._id})
        .then((user) => {
            if (req.user.role === 'admin') {
                next();
            }
            else {
                return res.status(401).json({
                    message: 'Not authorized to perform this operation'
                });
            }
        }, (err) => next(err))
        .catch((err) => next(err))
};

exports.verifyOwner = (req, res, next) => {
    User.findOne({_id: req.user._id})
        .then((user) => {
            if (req.user.role === 'owner') {
                next();
            }
            else {
                return res.status(401).json({
                    message: 'Not authorized to perform this operation'
                });
            }
        }, (err) => next(err))
        .catch((err) => next(err))
};